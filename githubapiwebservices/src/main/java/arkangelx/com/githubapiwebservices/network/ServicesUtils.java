package arkangelx.com.githubapiwebservices.network;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.toolbox.HttpHeaderParser;

import org.apache.http.NameValuePair;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;

public class ServicesUtils {

    public static final String STATUS_SUCCESS = "\"status\":\"success\"";
    public static final String DATA_START_CHAR_SEQ = "\"data\":";
    public static final String FIRST_DATA_CHAR_SQUARE_BRACKET = "[";
    public static final String LAST_DATA_CHAR_SQUARE_BRACKET = "]";
    public static final String LAST_DATA_CHAR_CURLY_BRACKET = "}";

    public static String getDataString(String serviceResponse) {
        String retString = serviceResponse;
        if (serviceResponse.contains(STATUS_SUCCESS)) {
            final int dataStart = serviceResponse.indexOf(DATA_START_CHAR_SEQ) + DATA_START_CHAR_SEQ.length();
            final char firstDataChar = serviceResponse.charAt(dataStart);
            int dataEnd;
            if (Character.toString(firstDataChar).equals(FIRST_DATA_CHAR_SQUARE_BRACKET)) {
                //its an array
                dataEnd = serviceResponse.lastIndexOf(LAST_DATA_CHAR_SQUARE_BRACKET) + 1;
            } else {
                //its an object
                dataEnd = serviceResponse.lastIndexOf(LAST_DATA_CHAR_CURLY_BRACKET);
            }
            retString = serviceResponse.substring(dataStart, dataEnd);
        }
        return retString;
    }

    public static String getGzipString(NetworkResponse response) throws IOException {
        String jsonString = "";
        GZIPInputStream gStream = new GZIPInputStream(new ByteArrayInputStream(response.data));
        InputStreamReader reader = new InputStreamReader(gStream);
        BufferedReader in = new BufferedReader(reader);
        String read;
        while ((read = in.readLine()) != null) {
            jsonString += read;
        }
        reader.close();
        in.close();
        gStream.close();
        return jsonString;
    }


    public static HashMap<String, String> ListToHashTable(List<NameValuePair> textBodies) {
        HashMap<String, String> params = new HashMap<String, String>();
        int size = textBodies.size();
        for (int i = 0; i < size; i++)
            params.put(textBodies.get(i).getName(), textBodies.get(i).getValue());
        return params;
    }

    public static Cache.Entry parseIgnoreCacheHeaders(NetworkResponse response) {
        long now = System.currentTimeMillis();
        Map<String, String> headers = response.headers;
        long serverDate = 0;
        String serverEtag = null;
        String headerValue;
        headerValue = headers.get("Date");
        if (headerValue != null) {
            serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
        }
        serverEtag = headers.get("ETag");
        final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
        final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
        final long softExpire = now + cacheHitButRefreshed;
        final long ttl = now + cacheExpired;
        Cache.Entry entry = new Cache.Entry();
        entry.data = response.data;
        entry.etag = serverEtag;
        entry.softTtl = softExpire;
        entry.ttl = ttl;
        entry.serverDate = serverDate;
        entry.responseHeaders = headers;
        return entry;
    }
}
