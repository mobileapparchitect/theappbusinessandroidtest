package arkangelx.com.githubapiwebservices.network;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.android.volley.VolleyError;
@JsonIgnoreProperties(ignoreUnknown = true)
public class ErrorResponse extends VolleyError {
    public String error;
    @JsonProperty("error_code")
    public int errorCode;
    @JsonProperty("error_field")
    public String errorField;
    public String status;
    public String exception;
}
