package arkangelx.com.githubapiwebservices.builders.post;


import com.android.volley.Request;

import arkangelx.com.githubapiwebservices.BaseJsonRequestBuilder;
import arkangelx.com.githubapiwebservices.TheAppBusinessTestApiRequest;
import arkangelx.com.githubapiwebservices.network.ServicesUtils;


abstract class PostRequestJsonBuilder extends BaseJsonRequestBuilder {
    

    protected PostRequestJsonBuilder() {
        setRequestBody(jsonObject.toString());
        setMethod(Request.Method.POST);
    }

    @Override
    public TheAppBusinessTestApiRequest build() {
        return new TheAppBusinessTestApiRequest(baseurl, method, listener, responseType, requestBody,errorListener,  ServicesUtils.ListToHashTable(params), tag, allowFromCache);

    }
}
