package arkangelx.com.githubapiwebservices.builders.get;

/**
 * Created by arkangel on 19/06/2016.
 */

import arkangelx.com.githubapiwebservices.builders.RequestMethods;
import arkangelx.com.githubapiwebservices.builders.RequestPaths;


public class GetGoogleSamplesRepoIssuesRequest extends GetRequestBuilder {
    // https://api.github.com/repos/googlesamples/android-ActiveNotifications/issues/8
    public GetGoogleSamplesRepoIssuesRequest(String repoName, String repoTitle, int index) {
        super();
        StringBuilder builder = new StringBuilder();
        builder.append("/" + repoName + "/" + repoTitle + RequestPaths.FIELD_REPO_ISSUES + "/" + String.valueOf(index));
        baseurl += RequestMethods.METHOD_REPOS + builder.toString();


    }

    public static GetGoogleSamplesRepoIssuesRequest newBuilder(String repoName, String repoTitle, int index) {
        return new GetGoogleSamplesRepoIssuesRequest(repoName, repoTitle, index);
    }
}


