package arkangelx.com.githubapiwebservices.builders.put;

import com.android.volley.Request;

import arkangelx.com.githubapiwebservices.BaseJsonRequestBuilder;
import arkangelx.com.githubapiwebservices.TheAppBusinessTestApiRequest;
import arkangelx.com.githubapiwebservices.network.ServicesUtils;


abstract class PutRequestJsonBuilder extends BaseJsonRequestBuilder {

    protected PutRequestJsonBuilder() {
        setMethod(Request.Method.PUT);
        setRequestBody(jsonObject.toString());
    }



    @Override
    public TheAppBusinessTestApiRequest build() {
        encodeParams();
        checkPreconditions();
        return new TheAppBusinessTestApiRequest(baseurl, method, listener, responseType, requestBody,errorListener,  ServicesUtils.ListToHashTable(params), tag, allowFromCache);

    }
}