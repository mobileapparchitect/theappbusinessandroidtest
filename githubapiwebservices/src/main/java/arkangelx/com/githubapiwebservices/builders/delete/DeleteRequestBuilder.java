package arkangelx.com.githubapiwebservices.builders.delete;


import com.android.volley.Request;

import arkangelx.com.githubapiwebservices.BaseJsonRequestBuilder;
import arkangelx.com.githubapiwebservices.TheAppBusinessTestApiRequest;
import arkangelx.com.githubapiwebservices.network.ServicesUtils;


abstract class DeleteRequestBuilder extends BaseJsonRequestBuilder {

    protected DeleteRequestBuilder() {
        setRequestBody(jsonObject.toString());
        setMethod(Request.Method.DELETE);
    }


    @Override
    public TheAppBusinessTestApiRequest build() {
        encodeParams();
        checkPreconditions();
        return new TheAppBusinessTestApiRequest(baseurl, method, listener, responseType, requestBody,errorListener,  ServicesUtils.ListToHashTable(params), tag, allowFromCache);

    }
}
