package arkangelx.com.githubapiwebservices.builders.get;




import com.android.volley.Request;

import arkangelx.com.githubapiwebservices.BaseJsonRequestBuilder;
import arkangelx.com.githubapiwebservices.TheAppBusinessTestApiRequest;
import arkangelx.com.githubapiwebservices.network.ServicesUtils;

public  class GetRequestBuilder extends BaseJsonRequestBuilder {
        public GetRequestBuilder() {
            setMethod(Request.Method.GET);
        }

        @Override
        public TheAppBusinessTestApiRequest build() {
            encodeParams();
            checkPreconditions();
            return new TheAppBusinessTestApiRequest(baseurl, method, listener, responseType, requestBody,errorListener,  ServicesUtils.ListToHashTable(params), tag, allowFromCache);

        }
    }