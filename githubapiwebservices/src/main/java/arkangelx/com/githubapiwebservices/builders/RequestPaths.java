package arkangelx.com.githubapiwebservices.builders;

public interface RequestPaths {

    public static final String FIELD_USERS = "/users";
    public static final String FIELD_GOOGLE_SAMPLE="/googlesamples";
    public static final String FIELD_REPO_ISSUES="/issues";


}