package arkangelx.com.githubapiwebservices.builders;


public interface HeaderFields {
    String ENCODING_GZIP = "gzip"; //NON-NLS
    String CONTENT_ENCODING = "Content-Encoding"; //NON-NLS
    String AUTHORIZATION_HEADER="Authorization"; //NON-NLS
    String CONTENT_TYPE ="Content-Type";
    String HEADER_ACCEPT = "Accept"; //NON-NLS

}
