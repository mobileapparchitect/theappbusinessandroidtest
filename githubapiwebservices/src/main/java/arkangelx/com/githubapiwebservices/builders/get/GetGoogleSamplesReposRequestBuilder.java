package arkangelx.com.githubapiwebservices.builders.get;

/**
 * Created by arkangel on 10/03/2016.
 */

import arkangelx.com.githubapiwebservices.builders.RequestPaths;
import arkangelx.com.githubapiwebservices.builders.RequestMethods;


public class GetGoogleSamplesReposRequestBuilder extends GetRequestBuilder {

    public GetGoogleSamplesReposRequestBuilder() {
        super();
        StringBuilder builder = new StringBuilder();
        builder.append(RequestPaths.FIELD_USERS);
        builder.append(RequestPaths.FIELD_GOOGLE_SAMPLE);
        baseurl +=builder.toString()+ RequestMethods.METHOD_REPOS;
    }

    public static GetGoogleSamplesReposRequestBuilder newBuilder() {
        return new GetGoogleSamplesReposRequestBuilder();
    }
}

