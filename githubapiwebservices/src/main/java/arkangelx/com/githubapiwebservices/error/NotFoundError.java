package arkangelx.com.githubapiwebservices.error;


public class NotFoundError extends GenericError {
    public NotFoundError(ErrorResponse response) {
        super(response);
    }

    public NotFoundError(String exceptionMessage) {
        super(exceptionMessage);
    }

    public NotFoundError(String exceptionMessage, Throwable reason) {
        super(exceptionMessage, reason);
    }

    public NotFoundError(Throwable cause) {
        super(cause);
    }
}
