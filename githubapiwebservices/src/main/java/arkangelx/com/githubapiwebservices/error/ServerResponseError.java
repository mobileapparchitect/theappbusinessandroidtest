package arkangelx.com.githubapiwebservices.error;


public class ServerResponseError extends GenericError {
    public ServerResponseError(ErrorResponse response) {
        super(response);
    }

    public ServerResponseError(String exceptionMessage) {
        super(exceptionMessage);
    }

    public ServerResponseError(String exceptionMessage, Throwable reason) {
        super(exceptionMessage, reason);
    }

    public ServerResponseError(Throwable cause) {
        super(cause);
    }

    public ServerResponseError(String response, String key) {
        super(response + " : " + key);
    }
}
