package arkangelx.com.githubapiwebservices.error;


public class UnauthorizedError extends GenericError {
    public UnauthorizedError(ErrorResponse response) {
        super(response);
    }

    public UnauthorizedError(String exceptionMessage) {
        super(exceptionMessage);
    }

    public UnauthorizedError(String exceptionMessage, Throwable reason) {
        super(exceptionMessage, reason);
    }

    public UnauthorizedError(Throwable cause) {
        super(cause);
    }

    public UnauthorizedError(String response, String key) {
        super(response + " : " + key);
    }
}
