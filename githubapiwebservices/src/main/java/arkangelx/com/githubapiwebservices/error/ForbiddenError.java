package arkangelx.com.githubapiwebservices.error;



public class ForbiddenError extends GenericError {
    public ForbiddenError(ErrorResponse response) {
        super(response);
    }

    public ForbiddenError(String exceptionMessage) {
        super(exceptionMessage);
    }

    public ForbiddenError(String exceptionMessage, Throwable reason) {
        super(exceptionMessage, reason);
    }

    public ForbiddenError(Throwable cause) {
        super(cause);
    }
}
