package arkangelx.com.githubapiwebservices.error;


public class ConflictError extends GenericError {
    public ConflictError(String response) {
        super(response);
    }

    public ConflictError(String exceptionMessage, Throwable reason) {
        super(exceptionMessage, reason);
    }

    public ConflictError(Throwable cause) {
        super(cause);
    }
}
