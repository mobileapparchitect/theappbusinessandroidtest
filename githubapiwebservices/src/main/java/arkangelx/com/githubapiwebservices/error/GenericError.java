package arkangelx.com.githubapiwebservices.error;

import com.android.volley.VolleyError;


public class GenericError extends VolleyError {


    public GenericError(String exceptionMessage) {
        super(exceptionMessage);
    }

    public GenericError(String exceptionMessage, Throwable reason) {
        super(exceptionMessage, reason);
    }

    public GenericError(Throwable cause) {
        super(cause);
    }
}
