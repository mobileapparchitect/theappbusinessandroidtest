package arkangelx.com.githubapiwebservices.error;


public class NetworkError extends GenericError {
    public NetworkError(ErrorResponse response) {
        super(response);
    }

    public NetworkError(String exceptionMessage) {
        super(exceptionMessage);
    }

    public NetworkError(String exceptionMessage, Throwable reason) {
        super(exceptionMessage, reason);
    }

    public NetworkError(Throwable cause) {
        super(cause);
    }
}
