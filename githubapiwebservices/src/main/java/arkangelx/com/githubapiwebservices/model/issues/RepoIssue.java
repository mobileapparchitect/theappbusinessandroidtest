
package arkangelx.com.githubapiwebservices.model.issues;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.hannesdorfmann.parcelableplease.annotation.Bagger;
import com.hannesdorfmann.parcelableplease.annotation.ParcelablePlease;

import java.util.ArrayList;
import java.util.List;
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@ParcelablePlease
public class RepoIssue extends BaseObservable implements Parcelable {

    @JsonProperty("url")
    public String url;
    @JsonProperty("repository_url")
    public String repositoryUrl;
    @JsonProperty("labels_url")
    public String labelsUrl;
    @JsonProperty("comments_url")
    public String commentsUrl;
    @JsonProperty("events_url")
    public String eventsUrl;
    @JsonProperty("html_url")
    public String htmlUrl;
    @JsonProperty("id")
    public int id;
    @JsonProperty("number")
    public int number;
    @JsonProperty("title")
    public String title;
    @JsonProperty("user")
    public User user;
    @JsonProperty("state")
    public String state;
    @JsonProperty("locked")
    public boolean locked;
    @JsonProperty("assignee")
    public Assignee assignee;
    @JsonProperty("milestone")
    public String milestone;
    @JsonProperty("comments")
    public int comments;
    @JsonProperty("created_at")
    public String createdAt;
    @JsonProperty("updated_at")
    public String updatedAt;
    @JsonProperty("closed_at")
    public String closedAt;
    @JsonProperty("pull_request")
    public PullRequest pullRequest;
    @JsonProperty("body")
    public String body;
    @JsonProperty("closed_by")
    public ClosedBy closedBy;

    public RepoIssue(){

    }

    protected RepoIssue(Parcel in) {
        url = in.readString();
        repositoryUrl = in.readString();
        labelsUrl = in.readString();
        commentsUrl = in.readString();
        eventsUrl = in.readString();
        htmlUrl = in.readString();
        id = in.readInt();
        number = in.readInt();
        title = in.readString();
        user = in.readParcelable(User.class.getClassLoader());

        state = in.readString();
        locked = in.readByte() != 0;
        assignee =  in.readParcelable(Assignee.class.getClassLoader());
        milestone = in.readString();
        comments = in.readInt();
        createdAt = in.readString();
        updatedAt = in.readString();
        closedAt = in.readString();
        pullRequest = in.readParcelable(PullRequest.class.getClassLoader());
        body = in.readString();
        closedBy = in.readParcelable(ClosedBy.class.getClassLoader());
    }

    public static final Creator<RepoIssue> CREATOR = new Creator<RepoIssue>() {
        @Override
        public RepoIssue createFromParcel(Parcel in) {
            return new RepoIssue(in);
        }

        @Override
        public RepoIssue[] newArray(int size) {
            return new RepoIssue[size];
        }
    };

    /**
     * 
     * @return
     *     The url
     */
    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    /**
     * 
     * @param url
     *     The url
     */
    @JsonProperty("url")
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * 
     * @return
     *     The repositoryUrl
     */
    @JsonProperty("repository_url")
    public String getRepositoryUrl() {
        return repositoryUrl;
    }

    /**
     * 
     * @param repositoryUrl
     *     The repository_url
     */
    @JsonProperty("repository_url")
    public void setRepositoryUrl(String repositoryUrl) {
        this.repositoryUrl = repositoryUrl;
    }

    /**
     * 
     * @return
     *     The labelsUrl
     */
    @JsonProperty("labels_url")
    public String getLabelsUrl() {
        return labelsUrl;
    }

    /**
     * 
     * @param labelsUrl
     *     The labels_url
     */
    @JsonProperty("labels_url")
    public void setLabelsUrl(String labelsUrl) {
        this.labelsUrl = labelsUrl;
    }

    /**
     * 
     * @return
     *     The commentsUrl
     */
    @JsonProperty("comments_url")
    public String getCommentsUrl() {
        return commentsUrl;
    }

    /**
     * 
     * @param commentsUrl
     *     The comments_url
     */
    @JsonProperty("comments_url")
    public void setCommentsUrl(String commentsUrl) {
        this.commentsUrl = commentsUrl;
    }

    /**
     * 
     * @return
     *     The eventsUrl
     */
    @JsonProperty("events_url")
    public String getEventsUrl() {
        return eventsUrl;
    }

    /**
     * 
     * @param eventsUrl
     *     The events_url
     */
    @JsonProperty("events_url")
    public void setEventsUrl(String eventsUrl) {
        this.eventsUrl = eventsUrl;
    }

    /**
     * 
     * @return
     *     The htmlUrl
     */
    @JsonProperty("html_url")
    public String getHtmlUrl() {
        return htmlUrl;
    }

    /**
     * 
     * @param htmlUrl
     *     The html_url
     */
    @JsonProperty("html_url")
    public void setHtmlUrl(String htmlUrl) {
        this.htmlUrl = htmlUrl;
    }

    /**
     * 
     * @return
     *     The id
     */
    @JsonProperty("id")
    public int getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    @JsonProperty("id")
    public void setId(int id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The number
     */
    @JsonProperty("number")
    public int getNumber() {
        return number;
    }

    /**
     * 
     * @param number
     *     The number
     */
    @JsonProperty("number")
    public void setNumber(int number) {
        this.number = number;
    }

    /**
     * 
     * @return
     *     The title
     */
    @Bindable
    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The title
     */
    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 
     * @return
     *     The user
     */
    @Bindable
    @JsonProperty("user")
    public User getUser() {
        return user;
    }

    /**
     * 
     * @param user
     *     The user
     */
    @JsonProperty("user")
    public void setUser(User user) {
        this.user = user;
    }


    /**
     * 
     * @return
     *     The state
     */
    @JsonProperty("state")
    public String getState() {
        return state;
    }

    /**
     * 
     * @param state
     *     The state
     */
    @JsonProperty("state")
    public void setState(String state) {
        this.state = state;
    }

    /**
     * 
     * @return
     *     The locked
     */
    @JsonProperty("locked")
    public boolean isLocked() {
        return locked;
    }

    /**
     * 
     * @param locked
     *     The locked
     */
    @JsonProperty("locked")
    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    /**
     * 
     * @return
     *     The assignee
     */
    @JsonProperty("assignee")
    public Object getAssignee() {
        return assignee;
    }

    /**
     * 
     * @param assignee
     *     The assignee
     */
    @JsonProperty("assignee")
    public void setAssignee(Assignee assignee) {
        this.assignee = assignee;
    }

    /**
     * 
     * @return
     *     The milestone
     */
    @JsonProperty("milestone")
    public String getMilestone() {
        return milestone;
    }

    /**
     * 
     * @param milestone
     *     The milestone
     */
    @JsonProperty("milestone")
    public void setMilestone(String milestone) {
        this.milestone = milestone;
    }

    /**
     * 
     * @return
     *     The comments
     */
    @JsonProperty("comments")
    public int getComments() {
        return comments;
    }

    /**
     * 
     * @param comments
     *     The comments
     */
    @JsonProperty("comments")
    public void setComments(int comments) {
        this.comments = comments;
    }

    /**
     * 
     * @return
     *     The createdAt
     */
    @JsonProperty("created_at")
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * 
     * @param createdAt
     *     The created_at
     */
    @JsonProperty("created_at")
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * 
     * @return
     *     The updatedAt
     */
    @JsonProperty("updated_at")
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     * 
     * @param updatedAt
     *     The updated_at
     */
    @JsonProperty("updated_at")
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * 
     * @return
     *     The closedAt
     */
    @JsonProperty("closed_at")
    public String getClosedAt() {
        return closedAt;
    }

    /**
     * 
     * @param closedAt
     *     The closed_at
     */
    @JsonProperty("closed_at")
    public void setClosedAt(String closedAt) {
        this.closedAt = closedAt;
    }

    /**
     * 
     * @return
     *     The pullRequest
     */
    @JsonProperty("pull_request")
    public PullRequest getPullRequest() {
        return pullRequest;
    }

    /**
     * 
     * @param pullRequest
     *     The pull_request
     */
    @JsonProperty("pull_request")
    public void setPullRequest(PullRequest pullRequest) {
        this.pullRequest = pullRequest;
    }

    /**
     * 
     * @return
     *     The body
     */
    @Bindable
    @JsonProperty("body")
    public String getBody() {
        return body;
    }

    /**
     * 
     * @param body
     *     The body
     */
    @JsonProperty("body")
    public void setBody(String body) {
        this.body = body;
    }

    /**
     * 
     * @return
     *     The closedBy
     */
    @JsonProperty("closed_by")
    public Object getClosedBy() {
        return closedBy;
    }

    /**
     * 
     * @param closedBy
     *     The closed_by
     */
    @JsonProperty("closed_by")
    public void setClosedBy(ClosedBy closedBy) {
        this.closedBy = closedBy;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(url);
        dest.writeString(repositoryUrl);
        dest.writeString(labelsUrl);
        dest.writeString(commentsUrl);
        dest.writeString(eventsUrl);
        dest.writeString(htmlUrl);
        dest.writeInt(id);
        dest.writeInt(number);
        dest.writeString(title);
        dest.writeString(state);
        dest.writeByte((byte) (locked ? 1 : 0));
        dest.writeInt(comments);
        dest.writeString(createdAt);
        dest.writeString(updatedAt);
        dest.writeParcelable(pullRequest, flags);
        dest.writeString(body);
    }
}
