package arkangelx.com.githubapiwebservices.model;



public class StringResponse {

    private final String responseString;

    public StringResponse(String response) {
        this.responseString = response;
    }

    public String getResponseString() {
        return responseString;
    }
}