
package arkangelx.com.githubapiwebservices.model.repo;


import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.hannesdorfmann.parcelableplease.annotation.ParcelablePlease;

//import org.parceler.Parcel;

//@Parcel(parcelsIndex = false)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@ParcelablePlease
public class Repo extends BaseObservable implements Parcelable {


    public Repo(){
        super();
    }

    @JsonProperty("id")
    public int id;
    @JsonProperty("name")
    public String name;

    public static final Creator<Repo> CREATOR = new Creator<Repo>() {
        @Override
        public Repo createFromParcel(Parcel in) {
            return new Repo(in);
        }

        @Override
        public Repo[] newArray(int size) {
            return new Repo[size];
        }
    };

    @Bindable
    public boolean is_private() {
        return _private;
    }

    @JsonProperty("full_name")
    public String fullName;
    @JsonProperty("owner")
    public Owner owner;
    @JsonProperty("public")
    public boolean _private;
    @JsonProperty("html_url")
    public String htmlUrl;
    @JsonProperty("description")
    public String description;
    @JsonProperty("fork")
    public boolean fork;
    @JsonProperty("url")
    public String url;
    @JsonProperty("forks_url")
    public String forksUrl;
    @JsonProperty("keys_url")
    public String keysUrl;
    @JsonProperty("collaborators_url")
    public String collaboratorsUrl;
    @JsonProperty("teams_url")
    public String teamsUrl;
    @JsonProperty("hooks_url")
    public String hooksUrl;
    @JsonProperty("issue_events_url")
    public String issueEventsUrl;
    @JsonProperty("events_url")
    public String eventsUrl;
    @JsonProperty("assignees_url")
    public String assigneesUrl;
    @JsonProperty("branches_url")
    public String branchesUrl;
    @JsonProperty("tags_url")
    public String tagsUrl;
    @JsonProperty("blobs_url")
    public String blobsUrl;
    @JsonProperty("git_tags_url")
    public String gitTagsUrl;
    @JsonProperty("git_refs_url")
    public String gitRefsUrl;
    @JsonProperty("trees_url")
    public String treesUrl;
    @JsonProperty("statuses_url")
    public String statusesUrl;
    @JsonProperty("languages_url")
    public String languagesUrl;
    @JsonProperty("stargazers_url")
    public String stargazersUrl;
    @JsonProperty("contributors_url")
    public String contributorsUrl;
    @JsonProperty("subscribers_url")
    public String subscribersUrl;
    @JsonProperty("subscription_url")
    public String subscriptionUrl;
    @JsonProperty("commits_url")
    public String commitsUrl;
    @JsonProperty("git_commits_url")
    public String gitCommitsUrl;
    @JsonProperty("comments_url")
    public String commentsUrl;
    @JsonProperty("issue_comment_url")
    public String issueCommentUrl;
    @JsonProperty("contents_url")
    public String contentsUrl;
    @JsonProperty("compare_url")
    public String compareUrl;
    @JsonProperty("merges_url")
    public String mergesUrl;
    @JsonProperty("archive_url")
    public String archiveUrl;
    @JsonProperty("downloads_url")
    public String downloadsUrl;
    @JsonProperty("issues_url")
    public String issuesUrl;
    @JsonProperty("pulls_url")
    public String pullsUrl;
    @JsonProperty("milestones_url")
    public String milestonesUrl;
    @JsonProperty("notifications_url")
    public String notificationsUrl;
    @JsonProperty("labels_url")
    public String labelsUrl;
    @JsonProperty("releases_url")
    public String releasesUrl;
    @JsonProperty("deployments_url")
    public String deploymentsUrl;
    @JsonProperty("created_at")
    public String createdAt;
    @JsonProperty("updated_at")
    public String updatedAt;
    @JsonProperty("pushed_at")
    public String pushedAt;
    @JsonProperty("git_url")
    public String gitUrl;
    @JsonProperty("ssh_url")
    public String sshUrl;
    @JsonProperty("clone_url")
    public String cloneUrl;
    @JsonProperty("svn_url")
    public String svnUrl;
    @JsonProperty("homepage")
    public String homepage;
    @JsonProperty("size")
    public int size;
    @JsonProperty("stargazers_count")
    public int stargazersCount;
    @JsonProperty("watchers_count")
    public int watchersCount;
    @JsonProperty("language")
    public String language;
    @JsonProperty("has_issues")
    public boolean hasIssues;
    @JsonProperty("has_downloads")
    public boolean hasDownloads;
    @JsonProperty("has_wiki")
    public boolean hasWiki;
    @JsonProperty("has_pages")
    public boolean hasPages;
    @JsonProperty("forks_count")
    public int forksCount;
    @JsonProperty("mirror_url")
    public String mirrorUrl;
    @JsonProperty("open_issues_count")
    public int openIssuesCount;
    @JsonProperty("forks")
    public int forks;
    @JsonProperty("open_issues")
    public int openIssues;
    @JsonProperty("watchers")
    public int watchers;
    @JsonProperty("default_branch")
    public String defaultBranch;

    protected Repo(android.os.Parcel in) {
        id = in.readInt();
        name = in.readString();
        fullName = in.readString();
        _private = in.readByte() != 0;
        htmlUrl = in.readString();
        description = in.readString();
        fork = in.readByte() != 0;
        url = in.readString();
        forksUrl = in.readString();
        keysUrl = in.readString();
        collaboratorsUrl = in.readString();
        teamsUrl = in.readString();
        hooksUrl = in.readString();
        issueEventsUrl = in.readString();
        eventsUrl = in.readString();
        assigneesUrl = in.readString();
        branchesUrl = in.readString();
        tagsUrl = in.readString();
        blobsUrl = in.readString();
        gitTagsUrl = in.readString();
        gitRefsUrl = in.readString();
        treesUrl = in.readString();
        statusesUrl = in.readString();
        languagesUrl = in.readString();
        stargazersUrl = in.readString();
        contributorsUrl = in.readString();
        subscribersUrl = in.readString();
        subscriptionUrl = in.readString();
        commitsUrl = in.readString();
        gitCommitsUrl = in.readString();
        commentsUrl = in.readString();
        issueCommentUrl = in.readString();
        contentsUrl = in.readString();
        compareUrl = in.readString();
        mergesUrl = in.readString();
        archiveUrl = in.readString();
        downloadsUrl = in.readString();
        issuesUrl = in.readString();
        pullsUrl = in.readString();
        milestonesUrl = in.readString();
        notificationsUrl = in.readString();
        labelsUrl = in.readString();
        releasesUrl = in.readString();
        deploymentsUrl = in.readString();
        createdAt = in.readString();
        updatedAt = in.readString();
        pushedAt = in.readString();
        gitUrl = in.readString();
        sshUrl = in.readString();
        cloneUrl = in.readString();
        svnUrl = in.readString();
        homepage = in.readString();
        size = in.readInt();
        stargazersCount = in.readInt();
        watchersCount = in.readInt();
        language = in.readString();
        hasIssues = in.readByte() != 0;
        hasDownloads = in.readByte() != 0;
        hasWiki = in.readByte() != 0;
        hasPages = in.readByte() != 0;
        forksCount = in.readInt();
        mirrorUrl = in.readString();
        openIssuesCount = in.readInt();
        forks = in.readInt();
        openIssues = in.readInt();
        watchers = in.readInt();
        defaultBranch = in.readString();
    }





    /**
     * 
     * @return
     *     The id
     */
    @JsonProperty("id")
    public int getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    @JsonProperty("id")
    public void setId(int id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The fullName
     */
    @JsonProperty("full_name")
    public String getFullName() {
        return fullName;
    }

    /**
     * 
     * @param fullName
     *     The full_name
     */
    @JsonProperty("full_name")
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    /**
     * 
     * @return
     *     The owner
     */
    @JsonProperty("owner")
    public Owner getOwner() {
        return owner;
    }

    /**
     * 
     * @param owner
     *     The owner
     */
    @JsonProperty("owner")
    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    /**
     * 
     * @return
     *     The _private
     */
    @JsonProperty("public")
    public boolean isPrivate() {
        return _private;
    }

    /**
     * 
     * @param _private
     *     The public
     */
    @JsonProperty("public")
    public void setPrivate(boolean _private) {
        this._private = _private;
    }

    /**
     * 
     * @return
     *     The htmlUrl
     */
    @JsonProperty("html_url")
    public String getHtmlUrl() {
        return htmlUrl;
    }

    /**
     * 
     * @param htmlUrl
     *     The html_url
     */
    @JsonProperty("html_url")
    public void setHtmlUrl(String htmlUrl) {
        this.htmlUrl = htmlUrl;
    }

    /**
     * 
     * @return
     *     The description
     */
    @JsonProperty("description")
    public Object getDescription() {
        return description;
    }

    /**
     * 
     * @param description
     *     The description
     */
    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 
     * @return
     *     The fork
     */
    @JsonProperty("fork")
    public boolean isFork() {
        return fork;
    }

    /**
     * 
     * @param fork
     *     The fork
     */
    @JsonProperty("fork")
    public void setFork(boolean fork) {
        this.fork = fork;
    }

    /**
     * 
     * @return
     *     The url
     */
    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    /**
     * 
     * @param url
     *     The url
     */
    @JsonProperty("url")
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * 
     * @return
     *     The forksUrl
     */
    @JsonProperty("forks_url")
    public String getForksUrl() {
        return forksUrl;
    }

    /**
     * 
     * @param forksUrl
     *     The forks_url
     */
    @JsonProperty("forks_url")
    public void setForksUrl(String forksUrl) {
        this.forksUrl = forksUrl;
    }

    /**
     * 
     * @return
     *     The keysUrl
     */
    @JsonProperty("keys_url")
    public String getKeysUrl() {
        return keysUrl;
    }

    /**
     * 
     * @param keysUrl
     *     The keys_url
     */
    @JsonProperty("keys_url")
    public void setKeysUrl(String keysUrl) {
        this.keysUrl = keysUrl;
    }

    /**
     * 
     * @return
     *     The collaboratorsUrl
     */
    @JsonProperty("collaborators_url")
    public String getCollaboratorsUrl() {
        return collaboratorsUrl;
    }

    /**
     * 
     * @param collaboratorsUrl
     *     The collaborators_url
     */
    @JsonProperty("collaborators_url")
    public void setCollaboratorsUrl(String collaboratorsUrl) {
        this.collaboratorsUrl = collaboratorsUrl;
    }

    /**
     * 
     * @return
     *     The teamsUrl
     */
    @JsonProperty("teams_url")
    public String getTeamsUrl() {
        return teamsUrl;
    }

    /**
     * 
     * @param teamsUrl
     *     The teams_url
     */
    @JsonProperty("teams_url")
    public void setTeamsUrl(String teamsUrl) {
        this.teamsUrl = teamsUrl;
    }

    /**
     * 
     * @return
     *     The hooksUrl
     */
    @JsonProperty("hooks_url")
    public String getHooksUrl() {
        return hooksUrl;
    }

    /**
     * 
     * @param hooksUrl
     *     The hooks_url
     */
    @JsonProperty("hooks_url")
    public void setHooksUrl(String hooksUrl) {
        this.hooksUrl = hooksUrl;
    }

    /**
     * 
     * @return
     *     The issueEventsUrl
     */
    @JsonProperty("issue_events_url")
    public String getIssueEventsUrl() {
        return issueEventsUrl;
    }

    /**
     * 
     * @param issueEventsUrl
     *     The issue_events_url
     */
    @JsonProperty("issue_events_url")
    public void setIssueEventsUrl(String issueEventsUrl) {
        this.issueEventsUrl = issueEventsUrl;
    }

    /**
     * 
     * @return
     *     The eventsUrl
     */
    @JsonProperty("events_url")
    public String getEventsUrl() {
        return eventsUrl;
    }

    /**
     * 
     * @param eventsUrl
     *     The events_url
     */
    @JsonProperty("events_url")
    public void setEventsUrl(String eventsUrl) {
        this.eventsUrl = eventsUrl;
    }

    /**
     * 
     * @return
     *     The assigneesUrl
     */
    @JsonProperty("assignees_url")
    public String getAssigneesUrl() {
        return assigneesUrl;
    }

    /**
     * 
     * @param assigneesUrl
     *     The assignees_url
     */
    @JsonProperty("assignees_url")
    public void setAssigneesUrl(String assigneesUrl) {
        this.assigneesUrl = assigneesUrl;
    }

    /**
     * 
     * @return
     *     The branchesUrl
     */
    @JsonProperty("branches_url")
    public String getBranchesUrl() {
        return branchesUrl;
    }

    /**
     * 
     * @param branchesUrl
     *     The branches_url
     */
    @JsonProperty("branches_url")
    public void setBranchesUrl(String branchesUrl) {
        this.branchesUrl = branchesUrl;
    }

    /**
     * 
     * @return
     *     The tagsUrl
     */
    @JsonProperty("tags_url")
    public String getTagsUrl() {
        return tagsUrl;
    }

    /**
     * 
     * @param tagsUrl
     *     The tags_url
     */
    @JsonProperty("tags_url")
    public void setTagsUrl(String tagsUrl) {
        this.tagsUrl = tagsUrl;
    }

    /**
     * 
     * @return
     *     The blobsUrl
     */
    @JsonProperty("blobs_url")
    public String getBlobsUrl() {
        return blobsUrl;
    }

    /**
     * 
     * @param blobsUrl
     *     The blobs_url
     */
    @JsonProperty("blobs_url")
    public void setBlobsUrl(String blobsUrl) {
        this.blobsUrl = blobsUrl;
    }

    /**
     * 
     * @return
     *     The gitTagsUrl
     */
    @JsonProperty("git_tags_url")
    public String getGitTagsUrl() {
        return gitTagsUrl;
    }

    /**
     * 
     * @param gitTagsUrl
     *     The git_tags_url
     */
    @JsonProperty("git_tags_url")
    public void setGitTagsUrl(String gitTagsUrl) {
        this.gitTagsUrl = gitTagsUrl;
    }

    /**
     * 
     * @return
     *     The gitRefsUrl
     */
    @JsonProperty("git_refs_url")
    public String getGitRefsUrl() {
        return gitRefsUrl;
    }

    /**
     * 
     * @param gitRefsUrl
     *     The git_refs_url
     */
    @JsonProperty("git_refs_url")
    public void setGitRefsUrl(String gitRefsUrl) {
        this.gitRefsUrl = gitRefsUrl;
    }

    /**
     * 
     * @return
     *     The treesUrl
     */
    @JsonProperty("trees_url")
    public String getTreesUrl() {
        return treesUrl;
    }

    /**
     * 
     * @param treesUrl
     *     The trees_url
     */
    @JsonProperty("trees_url")
    public void setTreesUrl(String treesUrl) {
        this.treesUrl = treesUrl;
    }

    /**
     * 
     * @return
     *     The statusesUrl
     */
    @JsonProperty("statuses_url")
    public String getStatusesUrl() {
        return statusesUrl;
    }

    /**
     * 
     * @param statusesUrl
     *     The statuses_url
     */
    @JsonProperty("statuses_url")
    public void setStatusesUrl(String statusesUrl) {
        this.statusesUrl = statusesUrl;
    }

    /**
     * 
     * @return
     *     The languagesUrl
     */
    @JsonProperty("languages_url")
    public String getLanguagesUrl() {
        return languagesUrl;
    }

    /**
     * 
     * @param languagesUrl
     *     The languages_url
     */
    @JsonProperty("languages_url")
    public void setLanguagesUrl(String languagesUrl) {
        this.languagesUrl = languagesUrl;
    }

    /**
     * 
     * @return
     *     The stargazersUrl
     */
    @JsonProperty("stargazers_url")
    public String getStargazersUrl() {
        return stargazersUrl;
    }

    /**
     * 
     * @param stargazersUrl
     *     The stargazers_url
     */
    @JsonProperty("stargazers_url")
    public void setStargazersUrl(String stargazersUrl) {
        this.stargazersUrl = stargazersUrl;
    }

    /**
     * 
     * @return
     *     The contributorsUrl
     */
    @JsonProperty("contributors_url")
    public String getContributorsUrl() {
        return contributorsUrl;
    }

    /**
     * 
     * @param contributorsUrl
     *     The contributors_url
     */
    @JsonProperty("contributors_url")
    public void setContributorsUrl(String contributorsUrl) {
        this.contributorsUrl = contributorsUrl;
    }

    /**
     * 
     * @return
     *     The subscribersUrl
     */
    @JsonProperty("subscribers_url")
    public String getSubscribersUrl() {
        return subscribersUrl;
    }

    /**
     * 
     * @param subscribersUrl
     *     The subscribers_url
     */
    @JsonProperty("subscribers_url")
    public void setSubscribersUrl(String subscribersUrl) {
        this.subscribersUrl = subscribersUrl;
    }

    /**
     * 
     * @return
     *     The subscriptionUrl
     */
    @JsonProperty("subscription_url")
    public String getSubscriptionUrl() {
        return subscriptionUrl;
    }

    /**
     * 
     * @param subscriptionUrl
     *     The subscription_url
     */
    @JsonProperty("subscription_url")
    public void setSubscriptionUrl(String subscriptionUrl) {
        this.subscriptionUrl = subscriptionUrl;
    }

    /**
     * 
     * @return
     *     The commitsUrl
     */
    @JsonProperty("commits_url")
    public String getCommitsUrl() {
        return commitsUrl;
    }

    /**
     * 
     * @param commitsUrl
     *     The commits_url
     */
    @JsonProperty("commits_url")
    public void setCommitsUrl(String commitsUrl) {
        this.commitsUrl = commitsUrl;
    }

    /**
     * 
     * @return
     *     The gitCommitsUrl
     */
    @JsonProperty("git_commits_url")
    public String getGitCommitsUrl() {
        return gitCommitsUrl;
    }

    /**
     * 
     * @param gitCommitsUrl
     *     The git_commits_url
     */
    @JsonProperty("git_commits_url")
    public void setGitCommitsUrl(String gitCommitsUrl) {
        this.gitCommitsUrl = gitCommitsUrl;
    }

    /**
     * 
     * @return
     *     The commentsUrl
     */
    @JsonProperty("comments_url")
    public String getCommentsUrl() {
        return commentsUrl;
    }

    /**
     * 
     * @param commentsUrl
     *     The comments_url
     */
    @JsonProperty("comments_url")
    public void setCommentsUrl(String commentsUrl) {
        this.commentsUrl = commentsUrl;
    }

    /**
     * 
     * @return
     *     The issueCommentUrl
     */
    @JsonProperty("issue_comment_url")
    public String getIssueCommentUrl() {
        return issueCommentUrl;
    }

    /**
     * 
     * @param issueCommentUrl
     *     The issue_comment_url
     */
    @JsonProperty("issue_comment_url")
    public void setIssueCommentUrl(String issueCommentUrl) {
        this.issueCommentUrl = issueCommentUrl;
    }

    /**
     * 
     * @return
     *     The contentsUrl
     */
    @JsonProperty("contents_url")
    public String getContentsUrl() {
        return contentsUrl;
    }

    /**
     * 
     * @param contentsUrl
     *     The contents_url
     */
    @JsonProperty("contents_url")
    public void setContentsUrl(String contentsUrl) {
        this.contentsUrl = contentsUrl;
    }

    /**
     * 
     * @return
     *     The compareUrl
     */
    @JsonProperty("compare_url")
    public String getCompareUrl() {
        return compareUrl;
    }

    /**
     * 
     * @param compareUrl
     *     The compare_url
     */
    @JsonProperty("compare_url")
    public void setCompareUrl(String compareUrl) {
        this.compareUrl = compareUrl;
    }

    /**
     * 
     * @return
     *     The mergesUrl
     */
    @JsonProperty("merges_url")
    public String getMergesUrl() {
        return mergesUrl;
    }

    /**
     * 
     * @param mergesUrl
     *     The merges_url
     */
    @JsonProperty("merges_url")
    public void setMergesUrl(String mergesUrl) {
        this.mergesUrl = mergesUrl;
    }

    /**
     * 
     * @return
     *     The archiveUrl
     */
    @JsonProperty("archive_url")
    public String getArchiveUrl() {
        return archiveUrl;
    }

    /**
     * 
     * @param archiveUrl
     *     The archive_url
     */
    @JsonProperty("archive_url")
    public void setArchiveUrl(String archiveUrl) {
        this.archiveUrl = archiveUrl;
    }

    /**
     * 
     * @return
     *     The downloadsUrl
     */
    @JsonProperty("downloads_url")
    public String getDownloadsUrl() {
        return downloadsUrl;
    }

    /**
     * 
     * @param downloadsUrl
     *     The downloads_url
     */
    @JsonProperty("downloads_url")
    public void setDownloadsUrl(String downloadsUrl) {
        this.downloadsUrl = downloadsUrl;
    }

    /**
     * 
     * @return
     *     The issuesUrl
     */
    @JsonProperty("issues_url")
    public String getIssuesUrl() {
        return issuesUrl;
    }

    /**
     * 
     * @param issuesUrl
     *     The issues_url
     */
    @JsonProperty("issues_url")
    public void setIssuesUrl(String issuesUrl) {
        this.issuesUrl = issuesUrl;
    }

    /**
     * 
     * @return
     *     The pullsUrl
     */
    @JsonProperty("pulls_url")
    public String getPullsUrl() {
        return pullsUrl;
    }

    /**
     * 
     * @param pullsUrl
     *     The pulls_url
     */
    @JsonProperty("pulls_url")
    public void setPullsUrl(String pullsUrl) {
        this.pullsUrl = pullsUrl;
    }

    /**
     * 
     * @return
     *     The milestonesUrl
     */
    @JsonProperty("milestones_url")
    public String getMilestonesUrl() {
        return milestonesUrl;
    }

    /**
     * 
     * @param milestonesUrl
     *     The milestones_url
     */
    @JsonProperty("milestones_url")
    public void setMilestonesUrl(String milestonesUrl) {
        this.milestonesUrl = milestonesUrl;
    }

    /**
     * 
     * @return
     *     The notificationsUrl
     */
    @JsonProperty("notifications_url")
    public String getNotificationsUrl() {
        return notificationsUrl;
    }

    /**
     * 
     * @param notificationsUrl
     *     The notifications_url
     */
    @JsonProperty("notifications_url")
    public void setNotificationsUrl(String notificationsUrl) {
        this.notificationsUrl = notificationsUrl;
    }

    /**
     * 
     * @return
     *     The labelsUrl
     */
    @JsonProperty("labels_url")
    public String getLabelsUrl() {
        return labelsUrl;
    }

    /**
     * 
     * @param labelsUrl
     *     The labels_url
     */
    @JsonProperty("labels_url")
    public void setLabelsUrl(String labelsUrl) {
        this.labelsUrl = labelsUrl;
    }

    /**
     * 
     * @return
     *     The releasesUrl
     */
    @JsonProperty("releases_url")
    public String getReleasesUrl() {
        return releasesUrl;
    }

    /**
     * 
     * @param releasesUrl
     *     The releases_url
     */
    @JsonProperty("releases_url")
    public void setReleasesUrl(String releasesUrl) {
        this.releasesUrl = releasesUrl;
    }

    /**
     * 
     * @return
     *     The deploymentsUrl
     */
    @JsonProperty("deployments_url")
    public String getDeploymentsUrl() {
        return deploymentsUrl;
    }

    /**
     * 
     * @param deploymentsUrl
     *     The deployments_url
     */
    @JsonProperty("deployments_url")
    public void setDeploymentsUrl(String deploymentsUrl) {
        this.deploymentsUrl = deploymentsUrl;
    }

    /**
     * 
     * @return
     *     The createdAt
     */
    @JsonProperty("created_at")
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * 
     * @param createdAt
     *     The created_at
     */
    @JsonProperty("created_at")
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * 
     * @return
     *     The updatedAt
     */
    @JsonProperty("updated_at")
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     * 
     * @param updatedAt
     *     The updated_at
     */
    @JsonProperty("updated_at")
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * 
     * @return
     *     The pushedAt
     */
    @JsonProperty("pushed_at")
    public String getPushedAt() {
        return pushedAt;
    }

    /**
     * 
     * @param pushedAt
     *     The pushed_at
     */
    @JsonProperty("pushed_at")
    public void setPushedAt(String pushedAt) {
        this.pushedAt = pushedAt;
    }

    /**
     * 
     * @return
     *     The gitUrl
     */
    @JsonProperty("git_url")
    public String getGitUrl() {
        return gitUrl;
    }

    /**
     * 
     * @param gitUrl
     *     The git_url
     */
    @JsonProperty("git_url")
    public void setGitUrl(String gitUrl) {
        this.gitUrl = gitUrl;
    }

    /**
     * 
     * @return
     *     The sshUrl
     */
    @JsonProperty("ssh_url")
    public String getSshUrl() {
        return sshUrl;
    }

    /**
     * 
     * @param sshUrl
     *     The ssh_url
     */
    @JsonProperty("ssh_url")
    public void setSshUrl(String sshUrl) {
        this.sshUrl = sshUrl;
    }

    /**
     * 
     * @return
     *     The cloneUrl
     */
    @JsonProperty("clone_url")
    public String getCloneUrl() {
        return cloneUrl;
    }

    /**
     * 
     * @param cloneUrl
     *     The clone_url
     */
    @JsonProperty("clone_url")
    public void setCloneUrl(String cloneUrl) {
        this.cloneUrl = cloneUrl;
    }

    /**
     * 
     * @return
     *     The svnUrl
     */
    @JsonProperty("svn_url")
    public String getSvnUrl() {
        return svnUrl;
    }

    /**
     * 
     * @param svnUrl
     *     The svn_url
     */
    @JsonProperty("svn_url")
    public void setSvnUrl(String svnUrl) {
        this.svnUrl = svnUrl;
    }

    /**
     * 
     * @return
     *     The homepage
     */
    @JsonProperty("homepage")
    public Object getHomepage() {
        return homepage;
    }

    /**
     * 
     * @param homepage
     *     The homepage
     */
    @JsonProperty("homepage")
    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    /**
     * 
     * @return
     *     The size
     */
    @JsonProperty("size")
    public int getSize() {
        return size;
    }

    /**
     * 
     * @param size
     *     The size
     */
    @JsonProperty("size")
    public void setSize(int size) {
        this.size = size;
    }

    /**
     * 
     * @return
     *     The stargazersCount
     */
    @Bindable
    @JsonProperty("stargazers_count")
    public int getStargazersCount() {
        return stargazersCount;
    }

    /**
     * 
     * @param stargazersCount
     *     The stargazers_count
     */
    @JsonProperty("stargazers_count")
    public void setStargazersCount(int stargazersCount) {
        this.stargazersCount = stargazersCount;
    }

    /**
     * 
     * @return
     *     The watchersCount
     */
    @Bindable
    @JsonProperty("watchers_count")
    public int getWatchersCount() {
        return watchersCount;
    }

    /**
     * 
     * @param watchersCount
     *     The watchers_count
     */
    @JsonProperty("watchers_count")
    public void setWatchersCount(int watchersCount) {
        this.watchersCount = watchersCount;
    }

    /**
     * 
     * @return
     *     The language
     */
    @JsonProperty("language")
    public String getLanguage() {
        return language;
    }

    /**
     * 
     * @param language
     *     The language
     */
    @JsonProperty("language")
    public void setLanguage(String language) {
        this.language = language;
    }

    /**
     * 
     * @return
     *     The hasIssues
     */
    @JsonProperty("has_issues")
    public boolean isHasIssues() {
        return hasIssues;
    }

    /**
     * 
     * @param hasIssues
     *     The has_issues
     */
    @JsonProperty("has_issues")
    public void setHasIssues(boolean hasIssues) {
        this.hasIssues = hasIssues;
    }

    /**
     * 
     * @return
     *     The hasDownloads
     */
    @JsonProperty("has_downloads")
    public boolean isHasDownloads() {
        return hasDownloads;
    }

    /**
     * 
     * @param hasDownloads
     *     The has_downloads
     */
    @JsonProperty("has_downloads")
    public void setHasDownloads(boolean hasDownloads) {
        this.hasDownloads = hasDownloads;
    }

    /**
     * 
     * @return
     *     The hasWiki
     */
    @JsonProperty("has_wiki")
    public boolean isHasWiki() {
        return hasWiki;
    }

    /**
     * 
     * @param hasWiki
     *     The has_wiki
     */
    @JsonProperty("has_wiki")
    public void setHasWiki(boolean hasWiki) {
        this.hasWiki = hasWiki;
    }

    /**
     * 
     * @return
     *     The hasPages
     */
    @JsonProperty("has_pages")
    public boolean isHasPages() {
        return hasPages;
    }

    /**
     * 
     * @param hasPages
     *     The has_pages
     */
    @JsonProperty("has_pages")
    public void setHasPages(boolean hasPages) {
        this.hasPages = hasPages;
    }

    /**
     * 
     * @return
     *     The forksCount
     */
    @Bindable
    @JsonProperty("forks_count")
    public int getForksCount() {
        return forksCount;
    }

    /**
     * 
     * @param forksCount
     *     The forks_count
     */
    @JsonProperty("forks_count")
    public void setForksCount(int forksCount) {
        this.forksCount = forksCount;
    }

    /**
     * 
     * @return
     *     The mirrorUrl
     */
    @JsonProperty("mirror_url")
    public Object getMirrorUrl() {
        return mirrorUrl;
    }

    /**
     * 
     * @param mirrorUrl
     *     The mirror_url
     */
    @JsonProperty("mirror_url")
    public void setMirrorUrl(String mirrorUrl) {
        this.mirrorUrl = mirrorUrl;
    }

    /**
     * 
     * @return
     *     The openIssuesCount
     */
    @Bindable
    @JsonProperty("open_issues_count")
    public int getOpenIssuesCount() {
        return openIssuesCount;
    }

    /**
     * 
     * @param openIssuesCount
     *     The open_issues_count
     */
    @JsonProperty("open_issues_count")
    public void setOpenIssuesCount(int openIssuesCount) {
        this.openIssuesCount = openIssuesCount;
    }

    /**
     * 
     * @return
     *     The forks
     */
    @JsonProperty("forks")
    public int getForks() {
        return forks;
    }

    /**
     * 
     * @param forks
     *     The forks
     */
    @JsonProperty("forks")
    public void setForks(int forks) {
        this.forks = forks;
    }

    /**
     * 
     * @return
     *     The openIssues
     */
    @JsonProperty("open_issues")
    public int getOpenIssues() {
        return openIssues;
    }

    /**
     * 
     * @param openIssues
     *     The open_issues
     */
    @JsonProperty("open_issues")
    public void setOpenIssues(int openIssues) {
        this.openIssues = openIssues;
    }

    /**
     * 
     * @return
     *     The watchers
     */
    @JsonProperty("watchers")
    public int getWatchers() {
        return watchers;
    }

    /**
     * 
     * @param watchers
     *     The watchers
     */
    @JsonProperty("watchers")
    public void setWatchers(int watchers) {
        this.watchers = watchers;
    }

    /**
     * 
     * @return
     *     The defaultBranch
     */
    @JsonProperty("default_branch")
    public String getDefaultBranch() {
        return defaultBranch;
    }

    /**
     * 
     * @param defaultBranch
     *     The default_branch
     */
    @JsonProperty("default_branch")
    public void setDefaultBranch(String defaultBranch) {
        this.defaultBranch = defaultBranch;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(fullName);
        dest.writeParcelable(owner, flags);
        dest.writeByte((byte) (_private ? 1 : 0));
        dest.writeString(htmlUrl);
        dest.writeString(description);
        dest.writeByte((byte) (fork ? 1 : 0));
        dest.writeString(url);
        dest.writeString(forksUrl);
        dest.writeString(keysUrl);
        dest.writeString(collaboratorsUrl);
        dest.writeString(teamsUrl);
        dest.writeString(hooksUrl);
        dest.writeString(issueEventsUrl);
        dest.writeString(eventsUrl);
        dest.writeString(assigneesUrl);
        dest.writeString(branchesUrl);
        dest.writeString(tagsUrl);
        dest.writeString(blobsUrl);
        dest.writeString(gitTagsUrl);
        dest.writeString(gitRefsUrl);
        dest.writeString(treesUrl);
        dest.writeString(statusesUrl);
        dest.writeString(languagesUrl);
        dest.writeString(stargazersUrl);
        dest.writeString(contributorsUrl);
        dest.writeString(subscribersUrl);
        dest.writeString(subscriptionUrl);
        dest.writeString(commitsUrl);
        dest.writeString(gitCommitsUrl);
        dest.writeString(commentsUrl);
        dest.writeString(issueCommentUrl);
        dest.writeString(contentsUrl);
        dest.writeString(compareUrl);
        dest.writeString(mergesUrl);
        dest.writeString(archiveUrl);
        dest.writeString(downloadsUrl);
        dest.writeString(issuesUrl);
        dest.writeString(pullsUrl);
        dest.writeString(milestonesUrl);
        dest.writeString(notificationsUrl);
        dest.writeString(labelsUrl);
        dest.writeString(releasesUrl);
        dest.writeString(deploymentsUrl);
        dest.writeString(createdAt);
        dest.writeString(updatedAt);
        dest.writeString(pushedAt);
        dest.writeString(gitUrl);
        dest.writeString(sshUrl);
        dest.writeString(cloneUrl);
        dest.writeString(svnUrl);
        dest.writeString(homepage);
        dest.writeInt(size);
        dest.writeInt(stargazersCount);
        dest.writeInt(watchersCount);
        dest.writeString(language);
        dest.writeByte((byte) (hasIssues ? 1 : 0));
        dest.writeByte((byte) (hasDownloads ? 1 : 0));
        dest.writeByte((byte) (hasWiki ? 1 : 0));
        dest.writeByte((byte) (hasPages ? 1 : 0));
        dest.writeInt(forksCount);
        dest.writeString(mirrorUrl);
        dest.writeInt(openIssuesCount);
        dest.writeInt(forks);
        dest.writeInt(openIssues);
        dest.writeInt(watchers);
        dest.writeString(defaultBranch);
    }
}
