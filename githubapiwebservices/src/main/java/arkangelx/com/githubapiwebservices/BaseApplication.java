package arkangelx.com.githubapiwebservices;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.provider.Settings;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.util.Log;


import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;


import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.Locale;

import arkangelx.com.githubapiwebservices.logs.LogConfig;
import arkangelx.com.githubapiwebservices.network.VolleySingleton;


public class BaseApplication extends MultiDexApplication {

    protected static BaseApplication singleton;
    public static ObjectMapper objectMapper;
    public static final String LONG_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static BaseApplication getInstance() {
        return singleton;
    }



    public static void setBaseUrl(String baseUrl) {
        BaseApplication.baseUrl = baseUrl;
    }

    public static String baseUrl;
    public static String getDeviceId() {
        return Settings.Secure.getString(singleton.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
    public static Context getAppContext() {
        return singleton.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        singleton = this;
        objectMapper = new ObjectMapper();
        objectMapper.setDateFormat(new SimpleDateFormat(LONG_DATE_FORMAT, getLocale()));
        objectMapper.configure(JsonParser.Feature.AUTO_CLOSE_SOURCE, true);
        setBaseUrl("https://api.github.com");
        initLog();
    }

    public void initLog() {
     arkangelx.com.githubapiwebservices.logs.Log.init(this, new LogConfig() {
            @Override
            public byte getLogLevel() {
                return arkangelx.com.githubapiwebservices.logs.Log.ALL;
            }

            @Override
            public HashSet<Class<?>> getClassesToMute() {
                return null;
            }

            @Override
            public HashSet<String> getPackagesToMute() {
                return null;
            }
        });
    }

    @Override
    public void onLowMemory() {
        Runtime.getRuntime().gc();
        arkangelx.com.githubapiwebservices.logs.Log.e("onLowMemory()");
        VolleySingleton.getInstance().cancelAll();
        super.onLowMemory();
    }

    public static ObjectMapper getObjectMapper() {
        return objectMapper;
    }


    public Locale getLocale() {
        return getResources().getConfiguration().locale;
    }

    public String getAppBaseUrl() {
        return baseUrl;
    }
}
