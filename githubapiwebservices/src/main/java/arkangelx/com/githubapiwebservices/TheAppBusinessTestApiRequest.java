package arkangelx.com.githubapiwebservices;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;
import com.google.common.base.Charsets;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import arkangelx.com.githubapiwebservices.builders.HeaderFields;
import arkangelx.com.githubapiwebservices.error.ConflictError;
import arkangelx.com.githubapiwebservices.error.ForbiddenError;
import arkangelx.com.githubapiwebservices.error.GenericError;
import arkangelx.com.githubapiwebservices.error.NetworkError;
import arkangelx.com.githubapiwebservices.error.NotFoundError;
import arkangelx.com.githubapiwebservices.error.ServerResponseError;
import arkangelx.com.githubapiwebservices.error.UnauthorizedError;
import arkangelx.com.githubapiwebservices.logs.Log;
import arkangelx.com.githubapiwebservices.model.StringResponse;
import arkangelx.com.githubapiwebservices.network.HttpsTrustManager;
import arkangelx.com.githubapiwebservices.network.ServicesUtils;
import arkangelx.com.githubapiwebservices.network.VolleySingleton;
import arkangelx.com.githubapiwebservices.utils.DeviceUtils;


public class TheAppBusinessTestApiRequest<T> extends JsonRequest<T> {

    protected static final int SOCKET_TIMEOUT_MS = 15000;

    private final Class<T> responseType;
    private final Response.Listener<T> listener;
    private final Map<String, String> params;
    private final boolean allowFromCache;
    private String requestBody;


    public TheAppBusinessTestApiRequest(String url,
                                        int method,
                                        Response.Listener<T> listener,
                                        Class<T> responseType,
                                        String requestBody,
                                        Response.ErrorListener errorListener,
                                        HashMap<String, String> params,
                                        String tag,
                                        boolean allowFromCache) {
        super(method, url, requestBody, listener, errorListener);
        super.setTag(tag);
        this.responseType = responseType;
        this.listener = listener;
        this.requestBody = requestBody;
        this.params = params;
        this.allowFromCache = allowFromCache;
        this.setRetryPolicy(new DefaultRetryPolicy(SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        setShouldCache(allowFromCache);
    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        if (getMethod() == Method.POST) {
            return params;
        } else {
            return new HashMap<>();
        }
    }



    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        HashMap<String, String> headers = new HashMap<>();
        headers.put(HeaderFields.HEADER_ACCEPT, getBodyContentType());
        headers.put(HeaderFields.CONTENT_TYPE, getBodyContentType() + ";charset=utf-8");
        return headers;
    }

    @Override
    public String getBodyContentType() {
        return "application/json";
    }

    @Override
    public byte[] getBody() {
        return requestBody.getBytes();
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        String jsonString;
        if (response.headers.containsKey(HeaderFields.CONTENT_ENCODING) &&
                response.headers.get(HeaderFields.CONTENT_ENCODING).equals(HeaderFields.ENCODING_GZIP)) {
            try {
                jsonString = ServicesUtils.getGzipString(response);
            } catch (IOException e) {
                e.printStackTrace();

                return Response.error(new ParseError(e));
            }
        } else {
            try {
                jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();

                return Response.error(new ParseError(e));
            }
        }
        try {
            jsonString = ServicesUtils.getDataString(jsonString);
            T object = null;

            if (responseType.getSimpleName().contentEquals(StringResponse.class.getSimpleName())) {
                object = (T) new StringResponse(jsonString);
            } else {
                object = BaseApplication.getObjectMapper().readValue(jsonString, responseType);
            }

            Log.i("responseType:" + responseType + " object type:" + object.getClass().getSimpleName());
            return Response.success(object,
                    ServicesUtils.parseIgnoreCacheHeaders(response));
        } catch (Exception e) {
            e.printStackTrace();
            return Response.error(new ParseError(e));
        }

    }
//TODO refactor this block of code, quite repetitive
    @Override
    protected VolleyError parseNetworkError(VolleyError volleyError) {
        String response = "";
        String key = null;
        int responseCode = volleyError.networkResponse.statusCode;
        if (volleyError.networkResponse != null) {
            try {

                response = new String(volleyError.networkResponse.data,
                        HttpHeaderParser.parseCharset(volleyError.networkResponse.headers));

                if (response.contentEquals("[]")) {
                    response = "";
                } else {
                    JSONObject jsonObject = null;
                    try {
                        JSONArray jsonArray = new JSONArray(response);
                        jsonObject = jsonArray.getJSONObject(0);
                    } catch (JSONException e1) {
                        try {
                            jsonObject = new JSONObject(response);
                        } catch (JSONException e2) {
                        }
                    }

                    if (jsonObject != null) {
                        if (jsonObject.has("message")) {
                            response = jsonObject.getString("message");
                        }

                        if (jsonObject.has("key")) {
                            key = jsonObject.getString("key");
                        }

                        if (jsonObject.has("errorCode")) {
                            response = jsonObject.getString("errorCode");
                        }

                        if (jsonObject.has("code")) {
                            response = jsonObject.getString("code");
                        }
                    }
                }

                switch (responseCode) {
                    case 404:
                        return new NotFoundError(response);
                    case 500:
                    case 503:
                        if (key == null) {
                            return new ServerResponseError(response);
                        }
                        return new ServerResponseError(response, key);
                    case 403:
                        return new ForbiddenError(response);
                    case 401:
                        if (key == null) {
                            return new UnauthorizedError(response);
                        }
                        return new UnauthorizedError(response, key);
                    case 409:
                        return new ConflictError(response);
                    default:
                        return new GenericError(response);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (volleyError instanceof TimeoutError) {
            return volleyError;
        } else {
            response = volleyError.getMessage();
        }

        return new NetworkError(response);
    }

    @Override
    protected void deliverResponse(T t) {
        listener.onResponse(t);
    }

    public void execute() {
        if (getUrl().startsWith("https")) {
            HttpsTrustManager.allowAllSSL();
        }
        if (allowFromCache) {
            Cache cache = VolleySingleton.getInstance().getRequestQueue().getCache();
            Cache.Entry entry = cache.get(getUrl());
            if (entry != null && !DeviceUtils.isConnected(BaseApplication.getAppContext())) {
                try {
                    String data = new String(entry.data, Charsets.UTF_8.name());
                    listener.onResponse(BaseApplication.getObjectMapper().readValue(data, responseType));
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        VolleySingleton.getInstance().getRequestQueue().cancelAll(getTag());
        VolleySingleton.getInstance().getRequestQueue().add(this);
    }
}
