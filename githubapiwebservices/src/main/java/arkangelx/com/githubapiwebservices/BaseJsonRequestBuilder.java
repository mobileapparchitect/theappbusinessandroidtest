package arkangelx.com.githubapiwebservices;

/**
 * Created by arkangel on 07/08/15.
 */

import com.android.volley.Response;
import com.google.common.base.Charsets;
import com.google.common.base.Preconditions;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;


public abstract class BaseJsonRequestBuilder {

    protected String baseurl; //REQUIRED
    protected JSONObject jsonObject;
    protected Response.Listener listener; //REQUIRED
    protected Class responseType;
    protected Response.ErrorListener errorListener; //REQUIRED
    protected int method; //REQUIRED
    protected List<NameValuePair> params; //REQUIRED
    protected String tag; //REQUIRED
    protected boolean allowFromCache; // OPTIONAL
    protected String requestBody;
    protected String userToken;
    protected boolean setTmeTokenFlag, setCookieFlag;

    protected BaseJsonRequestBuilder() {
        baseurl = BaseApplication.getInstance().getAppBaseUrl();
        method = -1;
        allowFromCache = false;
        jsonObject = new JSONObject();
        params = new LinkedList<>();

    }

    public BaseJsonRequestBuilder setResponseListener(Response.Listener listener, Class responseType) {
        this.listener = listener;
        this.responseType = responseType;
        return this;
    }

    public BaseJsonRequestBuilder setErrorListener(Response.ErrorListener errorListener) {
        this.errorListener = errorListener;
        return this;
    }

    protected BaseJsonRequestBuilder setMethod(int method) {
        this.method = method;
        return this;
    }

    public BaseJsonRequestBuilder setTag(String tag) {
        this.tag = tag;
        return this;
    }

    public BaseJsonRequestBuilder setRequestBody(String requestBody) {
        this.requestBody = requestBody;
        return this;
    }
    public BaseJsonRequestBuilder allowCache(boolean allowFromCache) {
         this.allowFromCache = allowFromCache;
        return this;
    }

    protected void encodeParams() {
        String paramString = URLEncodedUtils.format(params, Charsets.UTF_8.name());
        if (!params.isEmpty()) {
            baseurl += "?" + paramString;
        }
    }



    protected void checkPreconditions() {
        Preconditions.checkNotNull(listener, "listener not set"); //NON-NLS
        Preconditions.checkNotNull(responseType, "responseType not set"); //NON-NLS
        Preconditions.checkNotNull(errorListener, "errorListener not set"); //NON-NLS
        Preconditions.checkNotNull(tag, "tag not set"); //NON-NLS
        Preconditions.checkNotNull(baseurl, "url not set"); //NON-NLS
        Preconditions.checkArgument(method != -1, "method not set"); //NON-NLS
    }

    public abstract TheAppBusinessTestApiRequest build();
}
