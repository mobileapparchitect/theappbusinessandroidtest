package arkangelx.com.theappbusinesstest.eventhandlers;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import arkangelx.com.githubapiwebservices.model.repo.Repo;
import arkangelx.com.theappbusinesstest.R;
import arkangelx.com.theappbusinesstest.views.fragments.RepoIssuesDetailFragment;
import arkangelx.com.theappbusinesstest.views.fragments.RepoListFragment;


public class RepoItemClickHandler {
    public Repo mRepo;

    public RepoItemClickHandler(Repo repo) {
        this.mRepo = repo;
    }

    public void onClickRepoItem(View view) {
        try {
            FragmentManager fragmentManager = ((AppCompatActivity)  view.getContext()).getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.fragment_container, RepoIssuesDetailFragment.getInstance(mRepo)).addToBackStack(RepoIssuesDetailFragment.class.getSimpleName()).commit();
        } catch (ClassCastException e) {
            Log.e(RepoItemClickHandler.class.getSimpleName(), "Can't get fragment manager");
        }

    }
}
