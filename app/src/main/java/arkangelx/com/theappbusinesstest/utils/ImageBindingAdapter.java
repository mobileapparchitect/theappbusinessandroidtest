package arkangelx.com.theappbusinesstest.utils;

import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public class ImageBindingAdapter {
    @BindingAdapter("android:src")
    public static void setImageUrl(ImageView view, String url) {
        if (url != null) {
            Glide.with(view.getContext()).load(url).into(view);
        }
    }
}
