package arkangelx.com.theappbusinesstest.utils;

/**
 * Created by arkangel on 19/06/2016.
 */

import android.databinding.BindingAdapter;
import android.text.Html;
import android.widget.TextView;

public class TextBindingAdapter {
    @BindingAdapter("android:text")
    public static void setTextData(TextView view, String text) {
        view.setText(Html.fromHtml(text));
    }
}
