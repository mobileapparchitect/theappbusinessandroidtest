package arkangelx.com.theappbusinesstest.broadcastreceiver;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;

import arkangelx.com.githubapiwebservices.utils.DeviceUtils;
import arkangelx.com.theappbusinesstest.eventbus.EventBusManager;
import arkangelx.com.theappbusinesstest.eventbus.RxBus;


public class NetworkConnectivityReceiver extends BroadcastReceiver {

    public static class Helper {

        private final Context mContext;

        public Helper(Context context) {
            mContext = context;
        }

        public void startReceiver() {
            setReceiverState(PackageManager.COMPONENT_ENABLED_STATE_ENABLED);
        }

        public void stopReceiver() {
            setReceiverState(PackageManager.COMPONENT_ENABLED_STATE_DISABLED);
        }

        private void setReceiverState(int state) {
            ComponentName receiver = new ComponentName(mContext, NetworkConnectivityReceiver.class);
            PackageManager pm = mContext.getPackageManager();
            pm.setComponentEnabledSetting(receiver, state, PackageManager.DONT_KILL_APP);
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.net.conn.CONNECTIVITY_CHANGE")) {

            RxBus<NetworkStatus> bus = EventBusManager.getInstance().getNetworkConnectivityChangeEventBus();
            bus.postIfChanged(DeviceUtils.isConnected(context) ? NetworkStatus.CONNECTED : NetworkStatus.DISCONNECTED);

            // Upon reconnecting, stop the receiver so we are not wasting battery. Android recommends
            // this approach or else it is too wasteful because we wake up the phone too many times.
            if (DeviceUtils.isConnected(context)) {
                new Helper(context).stopReceiver();
            }
        }
    }

    public enum NetworkStatus {
        CONNECTED,
        DISCONNECTED
    }
}
