package arkangelx.com.theappbusinesstest.eventbus;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.subjects.PublishSubject;
import rx.subjects.SerializedSubject;
import rx.subjects.Subject;

public class RxBus<T> {

    private final Subject<T, T> mBus;
    private T mLastEvent;

    public RxBus() {
        this(new SerializedSubject<T, T>(PublishSubject.<T>create()));
    }

    public RxBus(Subject<T, T> bus) {
        mBus = bus;
        mBus.subscribeOn(AndroidSchedulers.mainThread());
        mBus.subscribe(new Action1<T>() {
            @Override
            public void call(T t) {
                mLastEvent = t;
            }
        });
    }

    public void post(T object) {
        mBus.onNext(object);
    }

    public void postIfChanged(T object) {
        if ( mLastEvent == null || !mLastEvent.equals(object)) {
            mBus.onNext(object);
        }
    }

    public Subscription subscribe(Action1<? super T> onNext) {
        return mBus.subscribe(onNext);
    }

    public T getLastEvent() {
        return mLastEvent;
    }

}