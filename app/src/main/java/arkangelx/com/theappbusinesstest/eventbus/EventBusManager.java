package arkangelx.com.theappbusinesstest.eventbus;

import arkangelx.com.theappbusinesstest.broadcastreceiver.NetworkConnectivityReceiver;

public class EventBusManager {

    private static final EventBusManager sInstance = new EventBusManager();
    private final RxBus<NetworkConnectivityReceiver.NetworkStatus> mNetworkConnectivityChangeEventBus = new RxBus<>();

    public static EventBusManager getInstance() {
        return sInstance;
    }


    public RxBus<NetworkConnectivityReceiver.NetworkStatus> getNetworkConnectivityChangeEventBus() {
        return mNetworkConnectivityChangeEventBus;
    }

}
