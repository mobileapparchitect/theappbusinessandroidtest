package arkangelx.com.theappbusinesstest.views.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hannesdorfmann.mosby.mvp.lce.MvpLceFragment;

import java.util.List;

import arkangelx.com.githubapiwebservices.model.repo.Repo;
import arkangelx.com.theappbusinesstest.R;
import arkangelx.com.theappbusinesstest.mosby_mvp.ErrorMessage;
import arkangelx.com.theappbusinesstest.mosby_mvp.presenters.GoogleSampleReposPresenter;
import arkangelx.com.theappbusinesstest.mosby_mvp.presenters.RepoPresenter;
import arkangelx.com.theappbusinesstest.mosby_mvp.view.RepoView;
import arkangelx.com.theappbusinesstest.adapters.ReposAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;


public class RepoListFragment
        extends MvpLceFragment<SwipeRefreshLayout, List<Repo>, RepoView, RepoPresenter>
        implements RepoView, SwipeRefreshLayout.OnRefreshListener {

    @Bind(R.id.recyclerView)
    RecyclerView recyclerView;

    ReposAdapter adapter;

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    public static RepoListFragment getInstance(){
        return new RepoListFragment();
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstance) {
        super.onViewCreated(view, savedInstance);
        ButterKnife.bind(this, view);
        // Setup contentView == SwipeRefreshView
        contentView.setOnRefreshListener(this);

        // Setup recycler view
        adapter = new ReposAdapter();
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);
        loadData(false);
    }

    @Override
    public void loadData(boolean pullToRefresh) {
        presenter.loadRepos(pullToRefresh);
    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return ErrorMessage.get(e, pullToRefresh, getActivity());

    }

    @Override
    public RepoPresenter createPresenter() {
        return new GoogleSampleReposPresenter();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_repos_layout, container, false);
    }


    @Override
    public void onRefresh() {
        loadData(true);
    }

    @Override
    public void showContent() {
        super.showContent();
        contentView.setRefreshing(false);
    }

    @Override
    public void showError(Throwable e, boolean pullToRefresh) {
        super.showError(e, pullToRefresh);
        contentView.setRefreshing(false);
    }

    @Override
    public void setData(List<Repo> data) {
        adapter.setData(data);
        adapter.notifyDataSetChanged();
    }


}