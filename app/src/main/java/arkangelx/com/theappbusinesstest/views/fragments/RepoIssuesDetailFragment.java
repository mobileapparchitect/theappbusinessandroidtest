package arkangelx.com.theappbusinesstest.views.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.hannesdorfmann.mosby.mvp.lce.MvpLceFragment;

import java.util.List;

import arkangelx.com.githubapiwebservices.model.issues.RepoIssue;
import arkangelx.com.githubapiwebservices.model.repo.Repo;
import arkangelx.com.theappbusinesstest.R;
import arkangelx.com.theappbusinesstest.mosby_mvp.ErrorMessage;
import arkangelx.com.theappbusinesstest.adapters.RepoDetailAdapter;
import arkangelx.com.theappbusinesstest.mosby_mvp.presenters.GoogleSampleReposIssuesPresenter;
import arkangelx.com.theappbusinesstest.mosby_mvp.presenters.RepoIssueDetailPresenter;
import arkangelx.com.theappbusinesstest.mosby_mvp.view.RepoIssueDetailView;
import butterknife.Bind;
import butterknife.ButterKnife;


public class RepoIssuesDetailFragment
        extends MvpLceFragment<SwipeRefreshLayout, List<RepoIssue>, RepoIssueDetailView, RepoIssueDetailPresenter>
        implements RepoIssueDetailView, SwipeRefreshLayout.OnRefreshListener {

    @Bind(R.id.recyclerView)
    RecyclerView recyclerView;
    private static final String REPO_KEY = "repoKey";
    RepoDetailAdapter repoDetailAdapter;
    private Repo mRepo;

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    public static RepoIssuesDetailFragment getInstance(Repo repo) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(REPO_KEY, repo);
        RepoIssuesDetailFragment detailFragment = new RepoIssuesDetailFragment();
        detailFragment.setArguments(bundle);
        return detailFragment;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstance) {
        super.onViewCreated(view, savedInstance);
        ButterKnife.bind(this, view);
        // Setup contentView == SwipeRefreshView
        contentView.setOnRefreshListener(this);
        Bundle bundle = getArguments();
        if (bundle != null) {
            mRepo = bundle.getParcelable(REPO_KEY);
        }
        loadData(false);
        // Setup recycler view
        repoDetailAdapter = new RepoDetailAdapter();
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(repoDetailAdapter);

    }


    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return ErrorMessage.get(e, pullToRefresh, getActivity());

    }

    @Override
    public RepoIssueDetailPresenter createPresenter() {
        return new GoogleSampleReposIssuesPresenter();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_repos_layout, container, false);
    }


    @Override
    public void onRefresh() {
        loadData(true);
    }

    @Override
    public void showContent() {
        super.showContent();
        contentView.setRefreshing(false);
    }

    @Override
    public void showError(Throwable e, boolean pullToRefresh) {
        super.showError(e, pullToRefresh);
        contentView.setRefreshing(false);
    }

    @Override
    public void setData(List<RepoIssue> data) {
        repoDetailAdapter.setData(data);
        repoDetailAdapter.notifyDataSetChanged();
    }


    @Override
    public void loadData(boolean pullToRefresh) {
        presenter.loadsRepoIssues(mRepo);
    }


    @Override
    public void showEmptySet() {
        Toast.makeText(getActivity(), "No Issues Attached To This Repo",Toast.LENGTH_LONG).show();
    }
}