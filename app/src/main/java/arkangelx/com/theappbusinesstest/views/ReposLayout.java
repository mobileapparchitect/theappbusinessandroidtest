package arkangelx.com.theappbusinesstest.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.hannesdorfmann.mosby.mvp.viewstate.ViewState;
import com.hannesdorfmann.mosby.mvp.viewstate.layout.MvpViewStateFrameLayout;
import com.hannesdorfmann.mosby.mvp.viewstate.lce.data.CastedArrayListLceViewState;

import java.util.List;

import arkangelx.com.githubapiwebservices.model.repo.Repo;
import arkangelx.com.theappbusinesstest.R;
import arkangelx.com.theappbusinesstest.mosby_mvp.ErrorMessage;
import arkangelx.com.theappbusinesstest.mosby_mvp.presenters.GoogleSampleReposPresenter;
import arkangelx.com.theappbusinesstest.mosby_mvp.presenters.RepoPresenter;
import arkangelx.com.theappbusinesstest.mosby_mvp.view.RepoView;
import arkangelx.com.theappbusinesstest.adapters.ReposAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ReposLayout extends MvpViewStateFrameLayout<RepoView, RepoPresenter>
    implements RepoView, SwipeRefreshLayout.OnRefreshListener {

  @Bind(R.id.loadingView)
  View loadingView;
  @Bind(R.id.errorView)
  TextView errorView;
  @Bind(R.id.contentView) SwipeRefreshLayout contentView;
  @Bind(R.id.recyclerView)
  RecyclerView recyclerView;

  private ReposAdapter adapter;

  public ReposLayout(Context context) {
    super(context);
  }

  public ReposLayout(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public ReposLayout(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  @TargetApi(21)
  public ReposLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
    super(context, attrs, defStyleAttr, defStyleRes);
  }

  @Override protected void onFinishInflate() {
    super.onFinishInflate();
    ButterKnife.bind(this, this);

    contentView.setOnRefreshListener(this);

    adapter = new ReposAdapter();
    recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    recyclerView.setAdapter(adapter);
  }

  @Override public RepoPresenter createPresenter() {
    return new GoogleSampleReposPresenter();
  }


   @Override public ViewState<RepoView> createViewState() {
      return new CastedArrayListLceViewState<List<Repo>, RepoView>();
    }

    @Override public CastedArrayListLceViewState<List<Repo>, RepoView> getViewState() {
      return (CastedArrayListLceViewState) super.getViewState();
    }

  @Override public void onNewViewStateInstance() {
    loadData(false);
  }

  @Override public void showLoading(boolean pullToRefresh) {
    errorView.setVisibility(View.GONE);

    if (pullToRefresh) {
      if (pullToRefresh && !contentView.isRefreshing()) {
        // Workaround for measure bug: https://code.google.com/p/android/issues/detail?id=77712
        contentView.post(new Runnable() {
          @Override public void run() {
            contentView.setRefreshing(true);
          }
        });
      }
      contentView.setVisibility(View.VISIBLE);
    } else {
      loadingView.setVisibility(View.VISIBLE);
      contentView.setVisibility(View.GONE);
    }
    getViewState().setStateShowLoading(pullToRefresh);
  }

  @Override public void showContent() {
    loadingView.setVisibility(View.GONE);
    errorView.setVisibility(View.GONE);
    contentView.setVisibility(View.VISIBLE);
    contentView.setRefreshing(false);
    getViewState().setStateShowContent(adapter.getData());
  }

  @Override public void showError(Throwable e, boolean pullToRefresh) {
    getViewState().setStateShowError(e, pullToRefresh);

    String msg = ErrorMessage.get(e, pullToRefresh, getContext());

    loadingView.setVisibility(View.GONE);
    if (pullToRefresh) {
      contentView.setRefreshing(false);
      if (!isRestoringViewState()) {
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
      }
    } else {
      contentView.setVisibility(View.GONE);
      errorView.setText(msg);
      errorView.setVisibility(View.VISIBLE);
    }
  }

  @Override
  public void setData(List<Repo> data) {
    adapter.setData(data);
    adapter.notifyDataSetChanged();
  }




  @Override public void loadData(boolean pullToRefresh) {
    presenter.loadRepos(pullToRefresh);
  }

  @Override public void onRefresh() {
    loadData(true);
  }

  @OnClick(R.id.errorView) public void onErrorViewClicked() {
    loadData(false);
  }

  @Override protected void onDetachedFromWindow() {
    super.onDetachedFromWindow();

  }
}