package arkangelx.com.theappbusinesstest.views.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import arkangelx.com.theappbusinesstest.R;
import arkangelx.com.theappbusinesstest.views.fragments.RepoListFragment;
import butterknife.Bind;
import butterknife.ButterKnife;


public class MainActivity extends AppCompatActivity {
    @Bind(R.id.toolbar)
    Toolbar toolbar;

    private FragmentManager fragmentManager;
    private ArrayList<String> data = new ArrayList<>();
    private int finalBill;
    private ArrayList<PhoneLog> phoneLogArrayList;
    private ArrayList<PhoneLog> sortedList;
    private ArrayList<String> callDurations;
    private Map<String, ArrayList<PhoneLog>> stringArrayListMap;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fragment_container, RepoListFragment.getInstance()).commit();
        stringArrayListMap = new HashMap<>();
        sortedList = new ArrayList<>();
        callDurations = new ArrayList<>();
        buildData();
        for (String string : data) {
            PhoneLog log = new PhoneLog(string);
            if (!callDurations.contains(log.getmCallPart())) {
                callDurations.add(log.getmCallPart());
            }
            if (!stringArrayListMap.containsKey(log.getmCallPart())) {
                phoneLogArrayList = new ArrayList<>();
                phoneLogArrayList.add(log);
                stringArrayListMap.put(log.getmCallPart(), phoneLogArrayList);
            } else {
                stringArrayListMap.get(log.getmCallPart()).add(log);
            }

        }

        Collections.sort(callDurations, new StringComparator());

        for (String string : callDurations) {
            List<PhoneLog> rowEntry = stringArrayListMap.get(string);
            if (rowEntry.size() > 1) {
                Collections.sort(rowEntry, new PhoneNumberTagComparator());
                for (int count = 0; count < rowEntry.size(); count++) {
                    if (count == 0) {
                        rowEntry.get(count).setApplyPromo(true);
                    } else {
                        rowEntry.get(count).setApplyPromo(false);
                    }
                }
                sortedList.addAll(rowEntry);
            } else {
                sortedList.addAll(rowEntry);
            }
        }

        for (PhoneLog phoneLog : sortedList) {
            finalBill += phoneLog.calculateCost();
        }

        Log.i("PhoneBill", String.valueOf(finalBill));


        System.out.println(String.valueOf(finalBill));


    }


    public ArrayList<String> buildData() {
        data.add("00:01:07,400-234-090");
        data.add("00:05:01,701-080-080");
        data.add("00:05:00,400-234-070");
        data.add("00:05:00,400-234-090");
        data.add("00:05:00,400-234-010");
        data.add("00:05:00,400-234-090");
        data.add("00:05:00,400-234-020");
        data.add("00:01:07,400-234-090");
        data.add("00:01:07,400-234-090");
        return data;
    }


    private class PhoneNumberTagComparator implements Comparator<PhoneLog> {

        @Override
        public int compare(PhoneLog lhs, PhoneLog rhs) {
            return ((Integer) lhs.getmPhonenumber().hashCode()).compareTo((Integer) rhs.getmPhonenumber().hashCode());
        }
    }


    private class StringComparator implements Comparator<String> {
        @Override
        public int compare(String lhs, String rhs) {
            return ((Integer) rhs.hashCode()).compareTo((Integer) lhs.hashCode());
        }
    }


    private class PhoneLog {

        private String mPhonenumber;
        private String mCallPart;
        private int mCalculateValue;
        private int mCost;
        private String phoneLog;
        private boolean applyPromo;
        private int callDuration;


        public int getCallDuration() {
            return callDuration;
        }


        public String getmCallPart() {
            return mCallPart;
        }

        public void setmCallPart(String mCallPart) {
            this.mCallPart = mCallPart;
        }


        public void setPhoneLog(String phoneLog) {
            this.phoneLog = phoneLog;
        }


        public void setCalculateValue(int calculateValue) {
            this.mCalculateValue = calculateValue;
        }


        public String getmPhonenumber() {
            return mPhonenumber;
        }

        public void setmPhonenumber(String mPhonenumber) {
            this.mPhonenumber = mPhonenumber;
        }


        public PhoneLog(String log) {
            setPhoneLog(log);
            extractData();
        }

        private void extractData() {
            int commaIndex = phoneLog.indexOf(",");
            String callPart = phoneLog.substring(0, commaIndex);
            setmCallPart(callPart);
            String phoneNumberPart = phoneLog.substring(++commaIndex, phoneLog.length()).replace("-", "");
            setmPhonenumber(phoneNumberPart);
        }


        public void setApplyPromo(boolean applyPromo) {
            this.applyPromo = applyPromo;
        }

        private int calculateCost() {
            int calculatedValue = 0;
            int mins, secs;
            String[] colons = getmCallPart().split(":");
            Log.i("", colons.toString());
            mins = Integer.parseInt(colons[1]);
            secs = Integer.parseInt(colons[2]);

            if (mins >= 5) {
                calculatedValue = (mins + secs) * 150;
            } else {
                calculatedValue = ((mins * 60) + secs) * 3;
            }
            callDuration = ((mins * 60) + secs);
            Log.i("", String.valueOf(calculatedValue));
            if (applyPromo) {
                setCalculateValue(0);
            } else {
                setCalculateValue(calculatedValue);
            }


            return calculatedValue;
        }
    }





}
