package arkangelx.com.theappbusinesstest.mosby_mvp.presenters;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import arkangelx.com.githubapiwebservices.builders.get.GetGoogleSamplesReposRequestBuilder;
import arkangelx.com.githubapiwebservices.logs.Log;
import arkangelx.com.githubapiwebservices.model.issues.RepoIssue;
import arkangelx.com.githubapiwebservices.model.repo.Repo;
import arkangelx.com.theappbusinesstest.mosby_mvp.view.RepoView;

public class GoogleSampleReposPresenter extends MvpBasePresenter<RepoView>
        implements RepoPresenter {

    private static final String TAG = GoogleSampleReposPresenter.class.getSimpleName();

    private int failingCounter = 0;


    public GoogleSampleReposPresenter() {
        Log.d(TAG + "constructor " + toString());
    }

    @Override
    public void loadRepos(final boolean pullToRefresh) {

        Log.d(TAG + "loadRepos(" + pullToRefresh + ")");

        Log.d(TAG + "showLoading(" + pullToRefresh + ")");

        getView().showLoading(pullToRefresh);

        GetGoogleSamplesReposRequestBuilder.newBuilder().setResponseListener(new Response.Listener<Repo[]>() {
            @Override
            public void onResponse(Repo[] repoResponse) {

                Log.i(GoogleSampleReposPresenter.class.getSimpleName() + repoResponse.toString());
                if (isViewAttached()) {
                    Log.d(TAG + "setData()");
                    Arrays.sort(repoResponse,new RepoIssueComparator());
                    getView().setData(new ArrayList<Repo>( Arrays.asList(repoResponse)));
                    Log.d(TAG + "showContent()");
                    getView().showContent();
                }

            }
        }, Repo[].class).setErrorListener(new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                if (isViewAttached()) {
                    Log.d(TAG + "showError(" + volleyError.getClass().getSimpleName() + " , " + pullToRefresh + ")");
                    getView().showError(volleyError, pullToRefresh);
                }


            }
        }).allowCache(true).
                setTag(Repo[].class.getSimpleName().toString()).build().execute();
    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);


    }

    @Override
    public void attachView(RepoView view) {
        super.attachView(view);
        Log.d(TAG + "attach view " + view.toString());
    }


    //sort by number issue count
    private class RepoIssueComparator implements Comparator<Repo> {

        @Override
        public int compare(Repo left, Repo right) {
            return ((Integer) right.getOpenIssuesCount()).compareTo((Integer) left.getOpenIssuesCount());

        }
    }
}