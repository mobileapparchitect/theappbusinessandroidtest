package arkangelx.com.theappbusinesstest.mosby_mvp.presenters;

import com.hannesdorfmann.mosby.mvp.MvpPresenter;

import arkangelx.com.theappbusinesstest.mosby_mvp.view.RepoView;

public interface RepoPresenter extends MvpPresenter<RepoView> {

  public void loadRepos(final boolean pullToRefresh);
}