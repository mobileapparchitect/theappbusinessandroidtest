package arkangelx.com.theappbusinesstest.mosby_mvp.view;

import com.hannesdorfmann.mosby.mvp.lce.MvpLceView;

import java.util.List;

import arkangelx.com.githubapiwebservices.model.issues.RepoIssue;

/**
 * Created by arkangel on 19/06/2016.
 */

public interface RepoIssueDetailView extends MvpLceView<List<RepoIssue>> {

    public void showEmptySet();
}
