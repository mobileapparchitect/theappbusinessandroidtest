package arkangelx.com.theappbusinesstest.mosby_mvp.view;

import com.hannesdorfmann.mosby.mvp.lce.MvpLceView;

import java.util.List;

import arkangelx.com.githubapiwebservices.model.repo.Repo;

public interface RepoView extends MvpLceView<List<Repo>> {
}