package arkangelx.com.theappbusinesstest.mosby_mvp;

import android.content.Context;

import arkangelx.com.theappbusinesstest.R;

public class ErrorMessage {

  public static String get(Throwable e, boolean pullToRefresh, Context c) {

    if (pullToRefresh) {
      return c.getString(R.string.error_message);
    } else {
      return c.getString(R.string.error_retry);
    }
  }
}