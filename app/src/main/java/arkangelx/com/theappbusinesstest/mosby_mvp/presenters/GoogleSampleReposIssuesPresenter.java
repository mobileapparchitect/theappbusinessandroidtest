package arkangelx.com.theappbusinesstest.mosby_mvp.presenters;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import java.util.ArrayList;

import arkangelx.com.githubapiwebservices.builders.get.GetGoogleSamplesRepoIssuesRequest;
import arkangelx.com.githubapiwebservices.logs.Log;
import arkangelx.com.githubapiwebservices.model.issues.RepoIssue;
import arkangelx.com.githubapiwebservices.model.repo.Repo;
import arkangelx.com.theappbusinesstest.mosby_mvp.view.RepoIssueDetailView;

/**
 * Created by arkangel on 19/06/2016.
 */
public class GoogleSampleReposIssuesPresenter extends MvpBasePresenter<RepoIssueDetailView>
        implements RepoIssueDetailPresenter {
    private ArrayList<RepoIssue> repoIssues;

    @Override
    public void loadsRepoIssues(final Repo repo) {
        repoIssues = new ArrayList<>();
        if (repo.getOpenIssues() == 0) {
            if (isViewAttached()) {
                getView().showEmptySet();
            }
            return;
        }


        for (int count = 0; count < repo.getOpenIssuesCount(); count++) {
            final int pos = count + 1;
            GetGoogleSamplesRepoIssuesRequest.newBuilder(repo.getOwner().getLogin(), repo.getName(), count + 1).setResponseListener(new Response.Listener<RepoIssue>() {
                @Override
                public void onResponse(RepoIssue repoIssue) {
                    Log.i(GetGoogleSamplesRepoIssuesRequest.class.getSimpleName() + repoIssue.toString());
                    repoIssues.add(repoIssue);
                    if (isViewAttached() && pos == repo.getOpenIssuesCount()) {
                        getView().setData(repoIssues);
                        getView().showContent();
                    }

                }
            }, RepoIssue.class).setErrorListener(new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    if (isViewAttached()) {
                        getView().showError(volleyError, true);
                    }


                }
            }).allowCache(true).
                    setTag(RepoIssue.class.getSimpleName().toString()).build().execute();


        }

    }

}
