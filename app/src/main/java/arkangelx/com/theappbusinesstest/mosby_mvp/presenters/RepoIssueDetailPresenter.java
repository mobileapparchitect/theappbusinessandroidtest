package arkangelx.com.theappbusinesstest.mosby_mvp.presenters;

import com.hannesdorfmann.mosby.mvp.MvpPresenter;

import arkangelx.com.githubapiwebservices.model.repo.Repo;
import arkangelx.com.theappbusinesstest.mosby_mvp.view.RepoIssueDetailView;

/**
 * Created by arkangel on 19/06/2016.
 */
public interface RepoIssueDetailPresenter extends MvpPresenter<RepoIssueDetailView> {
     void loadsRepoIssues(Repo repo);
}

