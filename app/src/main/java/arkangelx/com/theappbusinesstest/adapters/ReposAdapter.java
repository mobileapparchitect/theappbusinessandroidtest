package arkangelx.com.theappbusinesstest.adapters;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import arkangelx.com.githubapiwebservices.model.repo.Repo;
import arkangelx.com.theappbusinesstest.BR;
import arkangelx.com.theappbusinesstest.R;
import arkangelx.com.theappbusinesstest.eventhandlers.RepoItemClickHandler;


public class ReposAdapter extends RecyclerView.Adapter<ReposAdapter.BindingHolder> {

    public List<Repo> getData() {
        return mRepos;
    }

    private List<Repo> mRepos;


    public ReposAdapter() {
    }

    @Override
    public BindingHolder onCreateViewHolder(ViewGroup parent, int type) {
        return new BindingHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.view_repo_item_list_row, parent, false));
    }

    public void setData(List<Repo> repoItems) {
        mRepos = repoItems;
    }

    @Override
    public void onBindViewHolder(BindingHolder holder, int position) {
        final Repo repo = mRepos.get(position);
        holder.getBinding().setVariable(BR.repoViewModel, repo);
        RepoItemClickHandler clickHandler = new RepoItemClickHandler(repo);
        holder.getBinding().setVariable(BR.repoItemClick, clickHandler);
        holder.getBinding().executePendingBindings();
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        if (mRepos != null && !mRepos.isEmpty()) {
            return mRepos.size();
        }
        return 0;
    }


    public static class BindingHolder extends RecyclerView.ViewHolder {
        private ViewDataBinding binding;

        public BindingHolder(View rowView) {
            super(rowView);
            binding = DataBindingUtil.bind(rowView);

        }

        public ViewDataBinding getBinding() {
            return binding;
        }
    }

}
