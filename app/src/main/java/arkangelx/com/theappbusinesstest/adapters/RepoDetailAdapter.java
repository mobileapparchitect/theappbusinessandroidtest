package arkangelx.com.theappbusinesstest.adapters;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import arkangelx.com.githubapiwebservices.model.issues.RepoIssue;
import arkangelx.com.theappbusinesstest.BR;
import arkangelx.com.theappbusinesstest.R;


public class RepoDetailAdapter extends RecyclerView.Adapter<RepoDetailAdapter.BindingHolder> {

    public List<RepoIssue> getData() {
        return repoIssueList;
    }

    private List<RepoIssue> repoIssueList;


    public RepoDetailAdapter() {
        repoIssueList = new ArrayList<>();
    }

    @Override
    public BindingHolder onCreateViewHolder(ViewGroup parent, int type) {
        return new BindingHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.view_repo_detail, parent, false));
    }

    public void setData(List<RepoIssue> repoIssues) {
        repoIssueList = repoIssues;
    }

    @Override
    public void onBindViewHolder(BindingHolder holder, int position) {
        final RepoIssue repo = repoIssueList.get(position);
        holder.getBinding().setVariable(BR.repoIssueViewModel, repo);
        holder.getBinding().setVariable(BR.repoUser, repo.user);
        holder.getBinding().executePendingBindings();
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        if (repoIssueList != null && !repoIssueList.isEmpty()) {
            return repoIssueList.size();
        }
        return 0;
    }


    public static class BindingHolder extends RecyclerView.ViewHolder {
        private ViewDataBinding binding;

        public BindingHolder(View rowView) {
            super(rowView);
            binding = DataBindingUtil.bind(rowView);

        }

        public ViewDataBinding getBinding() {
            return binding;
        }
    }

}
