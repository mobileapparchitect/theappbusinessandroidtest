package arkangelx.com.theappbusinesstest;


import arkangelx.com.githubapiwebservices.BaseApplication;
import arkangelx.com.theappbusinesstest.broadcastreceiver.NetworkConnectivityReceiver;
import arkangelx.com.theappbusinesstest.eventbus.EventBusManager;
import rx.functions.Action1;

public class AppBusinessTestMainApplication extends BaseApplication {

    private static AppBusinessTestMainApplication singleton;
    public static AppBusinessTestMainApplication getInstance() {
        return singleton;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        singleton = this;
        // Setup network connectivity listener to start the broadcast receiver on disconnection.
        observeNetworkDisconnectionsToStartBroadcastReceiver();

    }
    private void observeNetworkDisconnectionsToStartBroadcastReceiver() {
        EventBusManager.getInstance().getNetworkConnectivityChangeEventBus().subscribe(new Action1<NetworkConnectivityReceiver.NetworkStatus>() {
            @Override
            public void call(NetworkConnectivityReceiver.NetworkStatus status) {
                if (status == NetworkConnectivityReceiver.NetworkStatus.DISCONNECTED) {
                    new NetworkConnectivityReceiver.Helper(getApplicationContext()).startReceiver();
                }
            }
        });
    }

}