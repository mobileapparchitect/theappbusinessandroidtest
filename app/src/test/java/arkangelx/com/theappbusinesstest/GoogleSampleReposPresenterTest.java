
package arkangelx.com.theappbusinesstest;


import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.junit.Test;

import java.util.List;

import arkangelx.com.githubapiwebservices.builders.get.GetGoogleSamplesReposRequestBuilder;
import arkangelx.com.githubapiwebservices.logs.Log;
import arkangelx.com.githubapiwebservices.model.repo.Repo;
import arkangelx.com.githubapiwebservices.utils.DeviceUtils;
import arkangelx.com.theappbusinesstest.mosby_mvp.presenters.GoogleSampleReposPresenter;
import arkangelx.com.theappbusinesstest.mosby_mvp.presenters.RepoPresenter;
import arkangelx.com.theappbusinesstest.mosby_mvp.view.RepoView;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class GoogleSampleReposPresenterTest {


    @Test
    public void presenterCallsGoogleSampleReposPresenterTest() {
        doReturn(true).when(DeviceUtils.isConnected(AppBusinessTestMainApplication.getAppContext()));

        GetGoogleSamplesReposRequestBuilder getGoogleSamplesReposRequestBuilder = mock(GetGoogleSamplesReposRequestBuilder.class);
        getGoogleSamplesReposRequestBuilder.setResponseListener(new Response.Listener<Repo[]>() {
            @Override
            public void onResponse(Repo[] repoResponse) {

                Log.i(GoogleSampleReposPresenter.class.getSimpleName() + repoResponse.toString());
            }
        }, Repo[].class).setErrorListener(new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {


            }
        }).allowCache(true).
                setTag(Repo[].class.getSimpleName().toString()).build().execute();

        RepoView view = mock(RepoView.class);
        GoogleSampleReposPresenter googleSampleReposPresenter = new GoogleSampleReposPresenter();
        googleSampleReposPresenter.attachView(view);
        googleSampleReposPresenter.loadRepos(true);
        verify(view).showContent();
    }

/*
    @Test
    public void presenterTest() {


        RepoView view = new RepoView() {

            boolean showLoadingCalled = false;
            boolean showErrorCalled = false;
            boolean showContentCalled = false;
            boolean setDataCalled = false;

            public void showLoading(boolean pullToRefresh) {
                showLoadingCalled = true;
            }

            public void setData(List<Repo> items) {
                //showItemsCalled = true;
            }

            @Override
            public void loadData(boolean pullToRefresh) {
                setDataCalled = true;
            }

            public void showError(Throwable t, boolean pullToRefresh) {
                showErrorCalled = true;
            }

            public void showContent() {
                showContentCalled = true;
            }
        };

        // Presenter to test
        RepoPresenter presenter = new GoogleSampleReposPresenter();
        presenter.attachView(view);  // Set the mock view
        presenter.loadRepos(true);

        // After having loaded items, we test if view methods gets invoked correctly
        assertTrue(view.showLoadingCalled);
        assertTrue(view.showContentCalled);
        assertTrue(view.setDataCalled);
        assertFalse(view.showErrorCalled);
    }*/
}